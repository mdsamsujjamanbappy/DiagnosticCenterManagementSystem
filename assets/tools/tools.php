<?php
class MyTools{
	function adminMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="doctor_list.php"><i class="fa fa-user-md"></i> Doctor List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="patient_list.php"><i class="fa fa-users"></i> Patient List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="receptionist_list.php"><i class="fa fa-users"></i> Receptionist List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="medical_store_attendant_list.php"><i class="fa fa-users"></i>M.Store Attendant List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="lab_attendant_list.php"><i class="fa fa-users"></i>Lab Attendant List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="referral_agent_list.php"><i class="fa fa-users"></i>Referral Agent List <span class="fa fa-chevron-right"></span></a></li>
	  <!-- <li><a href="medicine_list.php"><i class="fa fa-medkit"></i>Medicine List <span class="fa fa-chevron-right"></span></a></li> -->
	  <li><a href="test_list.php"><i class="fa fa-list"></i>Test List <span class="fa fa-chevron-right"></span></a></li>
	  
	  <li><a><i class="fa fa-bar-chart" ></i> Reports  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="search_patient_history.php">Search Patient History</a></li>
	      <li><a href="search_doctor_history.php">Search Doctor History</a></li>
	      <li><a href="search_test_history.php">Search Test History</a></li>
	      <li><a href="todays_report.php">Today's History</a></li>
	      <li><a href="date_wise_report.php">Date Wise Report</a></li>
	      <li><a href="referral_agent_wise_report.php">Referral Agent Wise Report</a></li>
	      <!-- <li><a href="receptionist_wise_report.php">Receptionist Wise Report</a></li> -->
	    </ul>
	  </li>

	  <li><a><i class="fa fa-cogs" ></i> Settings  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="department_list.php">Manage Department</a></li>
	      <li><a href="test_category_list.php">Manage Test Category</a></li>
	      <li><a href="dbbackup.php">Database Backup</a></li>
	    </ul>
	  </li>
	  <li><a href=""></a></li>

	</ul>
	<?php 
	}

	function recpMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
	 
	  <li><a><i class="fa fa-h-square" ></i>Patient Appointment  <span class="fa fa-chevron-down"></span></a>
		<ul class="nav child_menu">
          <li><a href="consultation.php">Consultation</a></li>
          <li><a href="op_procedure.php">LAB Consultation</a></li>
	    </ul>
	  </li>
	  <li><a href="patient_profile.php"><i class="fa fa-user"></i>Patient Profile <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="patient_list.php"><i class="fa fa-users"></i>Patient List<span class="fa fa-chevron-right"></span></a></li>
	  <li><a><i class="fa fa-money" ></i> Patient Payments  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="payment_history.php">Payment History</a></li>
	      <li><a href="receive_payment.php">Receive Payment</a></li>
	     </ul>
	  </li>
	  <!-- <li><a href="medicine_list.php"><i class="fa fa-medkit"></i>Medicine List <span class="fa fa-chevron-right"></span></a></li> -->
	  <li><a href="test_list.php"><i class="fa fa-subway"></i>Test List <span class="fa fa-chevron-right"></span></a></li>
	  
	  <li><a><i class="fa fa-hospital-o" ></i> Hospital Directory  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="doctor_list.php">Doctor List</a></li>
	      <li><a href="receptionist_list.php">Receptionist List</a></li>
	      <li><a href="medical_store_attendant_list.php">M.Store Attendant List</a></li>
	      <li><a href="lab_attendant_list.php">Lab Attendant List</a></li>
	      <li><a href="referral_agent_list.php">Referral agent</a></li>
	    </ul>
	  </li>

	  <li><a href="tools.php"><i class="fa fa-cubes"></i> Tools <span class="fa fa-chevron-right"></span></a></li>

	</ul>
	<?php 
	}

	function labaMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
	
	  <li><a href="patient_profile.php"><i class="fa fa-user"></i>Patient Profile <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="patient_list.php"><i class="fa fa-users"></i>Patient List<span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="test_list.php"><i class="fa fa-subway"></i>Test List <span class="fa fa-chevron-right"></span></a></li>
	  
	  <li><a><i class="fa fa-hospital-o" ></i> Hospital Directory  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="doctor_list.php">Doctor List</a></li>
	      <li><a href="receptionist_list.php">Receptionist List</a></li>
	      <!-- <li><a href="medical_store_attendant_list.php">M.Store Attendant List</a></li> -->
	      <li><a href="lab_attendant_list.php">Lab Attendant List</a></li>
	    </ul>
	  </li>

	</ul>
	<?php 
	}


	function docMenu(){
	?>
	<ul class="nav side-menu">
	  <li><a href="dashboard.php"><i class="fa fa-home"></i> Dashboard <span class="fa fa-chevron-right"></span></a></li>
	
	  <li><a href="patient_profile.php"><i class="fa fa-bed"></i>Patient Profile <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="patient_list.php"><i class="fa fa-users"></i>Patient List<span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="test_list.php"><i class="fa fa-subway"></i>Test List <span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="appointment_list.php"><i class="fa fa-subway"></i>Pending Appointment<span class="fa fa-chevron-right"></span></a></li>
	  <li><a href="myprofile.php"><i class="fa fa-user"></i>My Profile<span class="fa fa-chevron-right"></span></a></li>
	  
	  <!-- <li><a><i class="fa fa-hospital-o" ></i> Hospital Directory  <span class="fa fa-chevron-down"></span></a>
	    <ul class="nav child_menu">
	      <li><a href="doctor_list.php">Doctor List</a></li>
	      <li><a href="receptionist_list.php">Receptionist List</a></li>
	      <li><a href="lab_attendant_list.php">Lab Attendant List</a></li>
	    </ul>
	  </li> -->

	</ul>
	<?php 
	}

	function developer(){

		echo "<p align='center'> Developed by <b>Far-East IT Solutions Limited</b></p>";
		
	}
	 
	function title(){
		echo "Hospital Management System";	
	}

	function CURRENCY(){
		echo "Taka";	
	}

	function docFullname(){
		echo $_SESSION['docHMSfname'];	
	}

	function recpFullname(){
		echo $_SESSION['recpHMSfname'];	
	}
	 
	function labaFullname(){
		echo $_SESSION['labaHMSfname'];	
	}

	function fullname(){
		echo $_SESSION['adHMSfname'];	
	}

	function exfullname(){
		echo $_SESSION['fexname'];	
	}

	function profileLink(){ ?>
	 <ul class="dropdown-menu dropdown-usermenu pull-right">
		 <!-- 
	        <li><a href="myprofile"> Profile</a></li>
	        <li><a href="help">Help</a></li> 
        -->
        <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
      </ul>
	<?php 
	 }
	 
	 function docImage(){
	 	return "../doctor_images/".$_SESSION['docHMSImage'];
	 }
	 
	 function recpImage(){
	 	return "../user_images/".$_SESSION['recpHMSImage'];
	 }

	 function labaImage(){
	 	return "../user_images/".$_SESSION['labaHMSImage'];
	 }

	 function userImage(){
	 	return "../user_images/".$_SESSION['adHMSImage'];
	 }

	 function exuserImage(){
	 	return "../user_images/".$_SESSION['exImage'];
	 }

	function invoiceHeader(){

		echo "<h3>Far-East IT Solutions Limited.</h3>";
		echo "<p style='margin-top:-14px;margin-bottom:-14px;'>
			House #51, Road #18 Sector #11 Uttara, Dhaka-1230.<br>
			Phone: +8801763346334<br>
			Email: info@feits.co</p>";
	}

	function invoiceHeaderExport(){

		return "<center>
			<p><span style='font-size:25px;'><b>Far-East IT Solutions Limited.</b></span><br />
			House #51, Road #18 Sector #11 Uttara, Dhaka-1230
			<br />
			Phone: +8801763346334
			<br />
			Email: info@feits.co
			</p> </center>";
		}

	function myFooter(){ ?>
        <footer>
          <div class="pull-left">
            Copyright &copy; <?php echo date('Y');?> | <strong>FEITS Physiotherapy Hospital</strong> 
          </div>
          <div class="pull-right" style='color:#000'>
            Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
          </div>
          <div class="clearfix"></div>
        </footer>
	<?php
	}

	function sidebar_footer(){ ?>
		 <!-- 
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="System Settings"  href="#">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a> 
              
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="logout.php">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="">
                <span class="glyphicon" aria-hidden="true"></span>
              </a> -->
	<?php
	}
} 
?>