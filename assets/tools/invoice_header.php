<?php
	function invoiceCompanyTitle(){
		return "FEITS Physiotherapy Hospital";
	}

	function invoiceCompanyaddress(){
		return "House #51, Road #18 Sector #11 Uttara, Dhaka-1230";
	}

	function invoiceCompanyPhone(){
		return "Phone: +8801763346334 ";
	}

	function invoiceCompanyEmail(){
		return "Email: info@feits.co";
	}

	function footerText1(){
		return "To enjoy the glow of good health, you must exercise.";
	}

	function footerText2(){
		return "Thank you very much.";
	}

	function mainLogo(){
		$url = "../fpdf/img/logo1.jpeg";
		return $url;
	}

	function rxImages(){
		$url = "../fpdf/img/rx.png";
		return $url;
	}

	function watermarkImages(){
		$url = "../fpdf/img/img2.png";
		return $url;
	}

?>