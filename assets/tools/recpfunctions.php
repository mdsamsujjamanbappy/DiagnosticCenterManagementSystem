<?php
require_once("controller.php");

class myFunctions extends MSBController{
	
	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  		return $this->getRowNumber($query);
	}
	
	/* Test Management Area*/

	function getTestList(){
  		$query ="SELECT * FROM tbtestinfo order by testName ASC";
  		return $this->getData($query);
	}

	function getTestDetails($id){
			$query ="SELECT ttci.testCategoryName testCategoryName,tti.* FROM tbtestinfo tti LEFT JOIN tbtestcategoryinfo ttci ON tti.testCategoryId = ttci.id WHERE tti.id='$id'";
	  		return $this->getData($query);
	}

	function getTestListById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbtestinfo WHERE id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	function getTestCategoryList(){
  		$query ="SELECT * FROM tbtestcategoryinfo order by testCategoryName ASC";
  		return $this->getData($query);
	}

	function getTestListByCatId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT ttci.testCategoryName,tti.* FROM tbtestinfo tti LEFT JOIN tbtestcategoryinfo ttci ON ttci.id=tti.testCategoryId WHERE tti.testCategoryId='$id'";
  		return $this->getData($query);
	}


	/* Doctor Area */
	
	function getDoctorList(){
  		$query ="SELECT tdoc.*,td.departmentName departmentName FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId order by tdoc.name ASC";
  		return $this->getData($query);
	}

	function getDoctorShift($did){
			$query ="SELECT * FROM tbdoctorshift WHERE doctorId = '" . $did . "'";
  		return $this->getData($query);
	}


	function getDoctorTimeSchedule($did){
			date_default_timezone_set('Asia/Dhaka');
			$date=date("Y-m-d");
			$query ="SELECT * FROM tbpatientappointment WHERE apDate >='$date' && doctorId = '$did' order by apDate asc";
  		return $this->getData($query);
	}

	function getDoctorInformationById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tdoc.*,td.* FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId WHERE tdoc.id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	// ************************** Appointment Area ************************************

	function savePatient($id,$name,$phone,$email,$address,$date,$occupation,$father,$gender,$blood,$age,$dob){
		$id=$this->makeSecure($id);
		$name=$this->makeSecure($name);
		$phone=$this->makeSecure($phone);
		$email=$this->makeSecure($email);
		$address=$this->makeSecure($address);
		$date=$this->makeSecure($date);
		$occupation=$this->makeSecure($occupation);
		$father=$this->makeSecure($father);
		$gender=$this->makeSecure($gender);
		$blood=$this->makeSecure($blood);
		$age=$this->makeSecure($age);
		$dob=$this->makeSecure($dob);
		
		$query="INSERT INTO tbpatient(id,name,phone,email,address,addDate,occupation,father,gender,blood,age,dob) VALUES('$id','$name','$phone','$email','$address','$date','$occupation','$father','$gender','$blood','$age','$dob')";

  		return $this->insertData($query);
	}

	function saveAppointment($id,$apDate,$apTime,$doctor_id,$appFee,$date,$recepId){
		$apDate=$this->makeSecure($apDate);
		$apTime=$this->makeSecure($apTime);
		$doctor_id=$this->makeSecure($doctor_id);
		$patientId=$this->makeSecure($id);
		$appFee=$this->makeSecure($appFee);
		$date=$this->makeSecure($date);
		$recepId=$this->makeSecure($recepId);
		
		$query="INSERT INTO tbpatientappointment(apDate,apTime,doctorId,patientId,amount,receptionistId,createDate) VALUES('$apDate','$apTime','$doctor_id','$patientId','$appFee','$recepId','$date')";

  		return $this->insertData($query);
	}

	function saveAppointmentPayment($id,$date,$appFee,$credit,$description){
		$id=$this->makeSecure($id);
		$date=$this->makeSecure($date);
		$appFee=$this->makeSecure($appFee);
		$credit=$this->makeSecure($credit);
		$description=$this->makeSecure($description);
		
		$query="INSERT INTO tbpatientpayment(patientId,pDate,amount,creditAmount,pPDescription) VALUES('$id','$date','$appFee','$credit','$description')";

	  	return $this->insertData($query);
	}

	function savePMedicalHistory($id,$hblood,$hcardiac,$hrheumatic,$hdiabetes,$hhapatitis,$hpregnancy,$hkidney,$hhypersensitivity,$hbloodDisorder,$hcancer){

		$id=$this->makeSecure($id);
		$hblood=$this->makeSecure($hblood);
		$hcardiac=$this->makeSecure($hcardiac);
		$hrheumatic=$this->makeSecure($hrheumatic);
		$hdiabetes=$this->makeSecure($hdiabetes);
		$hhapatitis=$this->makeSecure($hhapatitis);
		$hpregnancy=$this->makeSecure($hpregnancy);
		$hkidney=$this->makeSecure($hkidney);
		$hhypersensitivity=$this->makeSecure($hhypersensitivity);
 		$hbloodDisorder=$this->makeSecure($hbloodDisorder);
 		$hcancer=$this->makeSecure($hcancer);
		
		$query="INSERT INTO tbmedicalhistory(patientId,hblood,hcardiac,hrheumatic,hdiabetes,hhapatitis, hpregnancy,hkidney, hhypersensitivity,hbloodDisorder,hcancer) VALUES('$id','$hblood','$hcardiac','$hrheumatic','$hdiabetes','$hhapatitis','$hpregnancy','$hkidney','$hhypersensitivity','$hbloodDisorder','$hcancer')";

  		return $this->insertData($query);
	}


	function savePArea($id,$pid,$area,$areaPosition){
		$id=$this->makeSecure($id);
		$pid=$this->makeSecure($pid);
		$area=$this->makeSecure($area);
		$areaPosition=$this->makeSecure($areaPosition);
		
		$query="UPDATE tbpatientbill SET area='$area', areaPosition='$areaPosition' WHERE id='$id' && patientId='$pid'";

  		return $this->updateData($query);
	}


	// ************************** Patient Testing Area ************************************

	function getPatientAppoinmentDetails($id){
		$query ="SELECT pa.*, d.name doctorname FROM tbpatientappointment pa LEFT JOIN tbdoctors d ON pa.doctorId=d.id where pa.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestList($id){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestDetailsById($tid,$pid){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.id='$tid' && pb.patientId='$pid'";
  		return $this->getData($query);
	}

	function getPatientPayment($pid){
		$query ="SELECT * FROM tbpatientpayment where patientId='$pid'";
  		return $this->getData($query);
	}

	function checkPatientId($pid){
		$query="SELECT * FROM tbpatient where id='$pid'";
  		return $this->getDataWithValue($query);
	}


	function getPatientListForAutocomplete($id){
			$query ="SELECT DISTINCT id FROM tbpatient where id LIKE '%$id%'";
  		return $this->getData($query);
	}


	// ************************** Patient Area ************************************

	function getPatientDetails($id){
			$query ="SELECT * FROM tbpatient where id='$id'";
  		return $this->getData($query);
	}

	function getPatientDetailsById($id){
			$query ="SELECT * FROM tbpatient where tmpid='$id'";
  		return $this->getData($query);
	}
	
	function getPatientMedicalHistory($id){
			$query ="SELECT * FROM tbmedicalhistory where patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientList(){
			$query ="SELECT * FROM tbpatient order by id DESC";
  		return $this->getData($query);
	}


	/* Users Area*/

	function getUserList($typeId){
		$typeId = $this->makeSecure($typeId);
  		$query ="SELECT * FROM tbusers WHERE userTypeId='$typeId' order by userFullName ASC";
  		return $this->getData($query);
	}

	function getUserListById($uid){
		$uid = $this->makeSecure($uid);
  		$query ="SELECT * FROM tbusers WHERE userid='$uid'";
  		return $this->getData($query);
	}

	// ******************** Patient Payment Area ************************

	function savePayment($id,$pid,$amount,$credit,$date,$description,$sts){
		$id=$this->makeSecure($id);
		$pid=$this->makeSecure($pid);
		$amount=$this->makeSecure($amount);
		$credit=$this->makeSecure($credit);
		$date=$this->makeSecure($date);
		$description=$this->makeSecure($description);
		$sts=$this->makeSecure($sts);
		
		$query="INSERT INTO tbpatientpayment(id,patientId,amount,creditAmount,pDate,pPDescription,tstatus) VALUES('$id','$pid','$amount','$credit','$date','$description','$sts')";

  		return $this->insertData($query);
	}

	function saveDuePayment($id,$pid,$amount,$credit,$date,$description,$recepId,$sts){
		$id=$this->makeSecure($id);
		$pid=$this->makeSecure($pid);
		$amount=$this->makeSecure($amount);
		$credit=$this->makeSecure($credit);
		$date=$this->makeSecure($date);
		$description=$this->makeSecure($description);
		$recepId=$this->makeSecure($recepId);
		$sts=$this->makeSecure($sts);
		
		$query="INSERT INTO tbpatientpayment(id,patientId,amount,creditAmount,pDate,pPDescription,userId,tstatus) VALUES('$id','$pid','$amount','$credit','$date','$description','$recepId','$sts')";

  		return $this->insertData($query);
	}

	// ************************** TEST Adding Area ************************************

	function getTmpOpTestList($id,$tmpid){
		$query ="SELECT tmp.*,tst.testName testName FROM tbtestTmp tmp LEFT JOIN tbtestinfo tst ON tmp.testId=tst.id  WHERE tmp.tmpid='$tmpid'&&patientId='$id'";
	  	return $this->getData($query);
	}

	function saveToTmp($tmpid,$pid,$test_id,$amount,$pbDate,$pbTime,$referredBy){
		$tmpid=$this->makeSecure($tmpid);
		$pid=$this->makeSecure($pid);
		$test_id=$this->makeSecure($test_id);
		$pbDate=$this->makeSecure($pbDate);
		$pbTime=$this->makeSecure($pbTime);
		$referredBy=$this->makeSecure($referredBy);
		
		$query="INSERT INTO tbtesttmp(tmpid,patientId,testId,amount,pbDate,pbTime,referredAgentId) VALUES('$tmpid','$pid','$test_id','$amount','$pbDate','$pbTime','$referredBy')";
  		return $this->insertData($query);
	}


	function saveToPb($pid,$test_id,$amount,$pbDate,$pbTime,$receptionistId,$date,$referredAgentId,$paymentId){
		$pid=$this->makeSecure($pid);
		$test_id=$this->makeSecure($test_id);
		$pbDate=$this->makeSecure($pbDate);
		$pbTime=$this->makeSecure($pbTime);
		$amount=$this->makeSecure($amount);
		$receptionistId=$this->makeSecure($receptionistId);
		$date=$this->makeSecure($date);
		$referredAgentId=$this->makeSecure($referredAgentId);
		$paymentId=$this->makeSecure($paymentId);
		
		$query="INSERT INTO tbpatienttestdetails(patientId,testId,testPrice,testDate,testTime,receptionistId,createDate,referredAgentId,paymentId) VALUES('$pid','$test_id','$amount','$pbDate','$pbTime','$receptionistId','$date','$referredAgentId','$paymentId')";
  		return $this->insertData($query);
	}

	function clearDataFromTmpOpTest($pid,$tmpid){
		$query ="DELETE FROM tbtesttmp WHERE tmpid='$tmpid'&&patientId='$pid'";
		return  $this->deleteData($query);
	}

	function getPatientMedicineDate($pid){
		$query ="SELECT distinct pdate FROM tbpatientmedicine WHERE patientId ='$pid' order by pdate desc";
  		return $this->getData($query);
	}

			
	function getPatientMedicine($pid,$pdate){
		$query ="SELECT tpm.* FROM tbpatientmedicine tpm WHERE tpm.patientId ='$pid' && tpm.pdate='$pdate'";
  		return $this->getData($query);
	}

	function getDoctorShiftById($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.doctorId='$id'";
  		return $this->getData($query);
	}

	function getDoctorShiftByShiftId($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.id='$id'";
  		return $this->getData($query);
	}


	function getReferralAgentList(){
  		$query ="SELECT * FROM tbreferral_agent";
  		return $this->getData($query);
	}

	function getReferralAgentDetailsById($id){
  		$query ="SELECT * FROM tbreferral_agent WHERE id='$id'";
  		return $this->getData($query);
	}

	function getReferredTestListById($id){
  		$query ="SELECT tptd.*,tst.testName testName FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tst ON tptd.testId=tst.id WHERE tptd.referredAgentId='$id'";
  		return $this->getData($query);
	}

	function saveReferralInformation($referralName,$referralPhone,$referralEmail,$referralAddress,$referralCperson,$referralCPhone,$addedBy){
		$referralName=$this->makeSecure($referralName);
		$referralPhone=$this->makeSecure($referralPhone);
		$referralEmail=$this->makeSecure($referralEmail);
		$referralAddress=$this->makeSecure($referralAddress);
		$referralCperson=$this->makeSecure($referralCperson);
		$referralCPhone=$this->makeSecure($referralCPhone);
		$addedBy=$this->makeSecure($addedBy);
		
		$query="INSERT INTO tbreferral_agent(name, address, phone, email, contactPersonName, contactPhone, addedBy) VALUES('$referralName','$referralAddress','$referralPhone','$referralEmail','$referralCperson','$referralCPhone','$addedBy')";
  		return $this->insertData($query);
	}

	function removeFromTmp($id){
		$query ="DELETE FROM tbtesttmp WHERE id='$id'";
		return  $this->deleteData($query);
	}

	
	function getPatientPaymentHistory($id){
			$query ="SELECT tbpp.*,tp.name patientName FROM tbpatientpayment tbpp LEFT JOIN tbpatient tp ON tbpp.patientId=tp.id WHERE patientId='$id'";
  		return $this->getData($query);
	}
	
	function getPatientPaymentHistoryById($id,$pid){
			$query ="SELECT tbpp.*,tp.name patientName FROM tbpatientpayment tbpp LEFT JOIN tbpatient tp ON tbpp.patientId=tp.id WHERE tbpp.patientId='$pid'&&tbpp.id='$id'";
  		return $this->getData($query);
	}

	function getPatientTestListByPayId($payid){
			$query ="SELECT tbptd.*,tti.testName testName FROM tbpatienttestdetails tbptd LEFT JOIN tbtestinfo tti ON tbptd.testId=tti.id WHERE tbptd.paymentId='$payid'";
  		return $this->getData($query);
	}

}
?>
