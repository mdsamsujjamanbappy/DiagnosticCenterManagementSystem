<?php
require_once("controller.php");

class myFunctions extends MSBController{
	
	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  	return $this->getRowNumber($query);
	}


	/* Department Area*/

	function getDepartment(){
  		$query ="SELECT * FROM tbdepartment order by departmentName ASC";
  		return $this->getData($query);
	}

	function getDepartmentById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbdepartment WHERE id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	function saveDepartment($name,$description){
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="INSERT INTO  tbdepartment(departmentName,departmentDescription) VALUES ('$name','$description')";
  		return $this->insertData($query);
	}

	function updateDepartment($id,$name,$description){
		$id = $this->makeSecure($id);
		$name = $this->makeSecure($name);
		$description = $this->makeSecure($description);
  		$query ="UPDATE tbdepartment SET departmentName='$name',departmentDescription='$description' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteDepartment($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbdepartment WHERE id= '$id'";
  		return $this->deleteData($query);
	}


	// ************************** Patient Area ************************************

	function getPatientDetails($id){
		$query ="SELECT * FROM tbpatient where id='$id'";
  		return $this->getData($query);
	}
	
	function getPatientMedicalHistory($id){
		$query ="SELECT * FROM tbmedicalhistory where patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientList(){
		$query ="SELECT * FROM tbpatient order by id DESC";
  		return $this->getData($query);
	}

	function getPatientPaymentHistory($id){
		$query ="SELECT tbpp.*,tp.name patientName FROM tbpatientpayment tbpp LEFT JOIN tbpatient tp ON tbpp.patientId=tp.id WHERE patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientListForAutocomplete($id){
		$query ="SELECT DISTINCT id FROM tbpatient where id LIKE '%$id%'";
  		return $this->getData($query);
	}


	// ************************** Patient Testing Area ************************************

	function getPatientAppoinmentDetails($id){
		$query ="SELECT pa.*, d.name doctorname FROM tbpatientappointment pa LEFT JOIN tbdoctors d ON pa.doctorId=d.id where pa.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestList($id){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestDetailsById($tid,$pid){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.id='$tid' && pb.patientId='$pid'";
  		return $this->getData($query);
	}

	function getPatientPayment($pid){
		$query ="SELECT * FROM tbpatientpayment where patientId='$pid'";
  		return $this->getData($query);
	}

	function checkPatientId($pid){
		$query="SELECT * FROM tbpatient where id='$pid'";
  		return $this->getDataWithValue($query);
	}

	function getPatientMedicineDate($pid){
		$query ="SELECT distinct pdate FROM tbpatientmedicine WHERE patientId ='$pid' order by pdate desc";
		return $this->getData($query);
	}

	function getPatientMedicine($pid,$pdate){
		$query ="SELECT tpm.* FROM tbpatientmedicine tpm WHERE tpm.patientId ='$pid' && tpm.pdate='$pdate'";
		return $this->getData($query);
	}


	/* Doctor Area*/

	function getDoctorList(){
  		$query ="SELECT tdoc.*,td.departmentName departmentName FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId order by tdoc.name ASC";
  		return $this->getData($query);
	}

	function getDoctorInformationById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tdoc.*,td.* FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId WHERE tdoc.id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	function saveDoctorInformation($name,$docDepartmentId,$designation,$qualification,$phone,$email,$password,$allocatedRoom,$address,$otherInfo,$registrationNumber,$doctorImage){

		$name = $this->makeSecure($name);
		$docDepartmentId = $this->makeSecure($docDepartmentId);
		$designation = $this->makeSecure($designation);
		$qualification = $this->makeSecure($qualification);
		$phone = $this->makeSecure($phone);
		$email = $this->makeSecure($email);
		$password = md5($this->makeSecure($password));
		$allocatedRoom = $this->makeSecure($allocatedRoom);
		$address = $this->makeSecure($address);
		$otherInfo = $this->makeSecure($otherInfo);
		$registrationNumber = $this->makeSecure($registrationNumber);
		$doctorImage = $this->makeSecure($doctorImage);

  		$query ="INSERT INTO tbdoctors(name, designation, docDepartmentId, address, image, qualification, othersInfo, phone, email, password, hospitalDetails, registrationNumber) VALUES('$name','$designation','$docDepartmentId','$address','$doctorImage','$qualification','$otherInfo','$phone','$email','$password','$allocatedRoom','$registrationNumber')";

  		return $this->insertData($query);
	}

	function updateDoctorInformation($did,$name,$docDepartmentId,$designation,$qualification,$phone,$email,$allocatedRoom,$address,$otherInfo,$registrationNumber,$doctorImage){

		$did = $this->makeSecure($did);
		$name = $this->makeSecure($name);
		$docDepartmentId = $this->makeSecure($docDepartmentId);
		$designation = $this->makeSecure($designation);
		$qualification = $this->makeSecure($qualification);
		$phone = $this->makeSecure($phone);
		$email = $this->makeSecure($email);
		$allocatedRoom = $this->makeSecure($allocatedRoom);
		$address = $this->makeSecure($address);
		$otherInfo = $this->makeSecure($otherInfo);
		$registrationNumber = $this->makeSecure($registrationNumber);
		$doctorImage = $this->makeSecure($doctorImage);

  		$query ="UPDATE tbdoctors SET name='$name', designation='$designation', docDepartmentId='$docDepartmentId', address='$address', image='$doctorImage', qualification='$qualification', othersInfo='$otherInfo', phone='$phone', email='$email', hospitalDetails='$allocatedRoom', registrationNumber='$registrationNumber' WHERE id='$did'";

  		return $this->updateData($query);
	}

	function deleteDoctorInformation($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbdoctors WHERE id = '$id'";
  		return $this->deleteData($query);
	}

	function getDoctorShiftById($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.doctorId='$id'";
  		return $this->getData($query);
	}

	function getDoctorShiftByShiftId($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.id='$id'";
  		return $this->getData($query);
	}

	function saveDoctorShift($did,$sDay,$sTime){
		$did = $this->makeSecure($did);
		$sDay = $this->makeSecure($sDay);
		$sTime = $this->makeSecure($sTime);
  		$query ="INSERT INTO tbdoctorshift (doctorId,day,time) VALUES('$did','$sDay','$sTime')";
  		return $this->insertData($query);
	}

	function updateDoctorShiftByShiftId($sid,$did,$sDay,$sTime){
		$sid = $this->makeSecure($sid);
		$did = $this->makeSecure($did);
		$sDay = $this->makeSecure($sDay);
		$sTime = $this->makeSecure($sTime);
  		$query ="UPDATE tbdoctorshift SET day='$sDay',time='$sTime' WHERE id= '$sid'&& doctorId= '$did'";
  		return $this->updateData($query);
	}

	function deleteDoctorShift($id,$did){
		$id = $this->makeSecure($id);
		$did = $this->makeSecure($did);
  		$query ="DELETE FROM tbdoctorshift WHERE id= '$id'&& doctorId= '$did'";
  		return $this->deleteData($query);
	}

	function checkDoctorId($id){
		$query="SELECT * FROM tbdoctors where id='$id'";
  		return $this->getDataWithValue($query);
	}

	function getPatientAppointmentListByDoctorId($id){
  		$query ="SELECT tdoc.name doctorName,tpa.*,tpt.name patientName FROM tbpatientappointment tpa LEFT JOIN tbdoctors tdoc ON tpa.doctorId=tdoc.id  LEFT JOIN tbpatient tpt ON tpa.patientId=tpt.id  WHERE tpa.doctorId='$id'";
  		return $this->getData($query);
	}

	/* Referral Agent Area */
	
	function getReferralAgentList(){
  		$query ="SELECT * FROM tbreferral_agent";
  		return $this->getData($query);
	}

	function getReferralAgentDetailsById($id){
  		$query ="SELECT * FROM tbreferral_agent WHERE id='$id'";
  		return $this->getData($query);
	}

	function getReferredTestListById($id,$fromDate,$toDate){
  		$query ="SELECT tptd.*,tst.testName testName,tra.commissionPercent commissionPercent FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tst ON tptd.testId=tst.id  LEFT JOIN tbreferral_agent tra ON tptd.referredAgentId=tra.id WHERE  (tptd.createDate BETWEEN '$fromDate' AND '$toDate') AND (tptd.referredAgentId='$id')";
  		return $this->getData($query);
	}

	function getReferredTestListById1($id){
  		$query ="SELECT tptd.*,tst.testName testName,tra.commissionPercent commissionPercent FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tst ON tptd.testId=tst.id  LEFT JOIN tbreferral_agent tra ON tptd.referredAgentId=tra.id WHERE tptd.referredAgentId='$id'";
  		return $this->getData($query);
	}

	function saveReferralInformation($referralName,$referralCPercent,$referralPhone,$referralEmail,$referralAddress,$referralCperson,$referralCPhone,$addedBy){
		$referralName=$this->makeSecure($referralName);
		$referralCPercent=$this->makeSecure($referralCPercent);
		$referralPhone=$this->makeSecure($referralPhone);
		$referralEmail=$this->makeSecure($referralEmail);
		$referralAddress=$this->makeSecure($referralAddress);
		$referralCperson=$this->makeSecure($referralCperson);
		$referralCPhone=$this->makeSecure($referralCPhone);
		$addedBy=$this->makeSecure($addedBy);
		
		$query="INSERT INTO tbreferral_agent(name, commissionPercent, address, phone, email, contactPersonName, contactPhone, addedBy) VALUES('$referralName','$referralCPercent','$referralAddress','$referralPhone','$referralEmail','$referralCperson','$referralCPhone','$addedBy')";
  		return $this->insertData($query);
	}

	function updateReferralInformation($id,$referralName,$referralCPercent,$referralPhone,$referralEmail,$referralAddress,$referralCperson,$referralCPhone){
		$id=$this->makeSecure($id);
		$referralName=$this->makeSecure($referralName);
		$referralCPercent=$this->makeSecure($referralCPercent);
		$referralPhone=$this->makeSecure($referralPhone);
		$referralEmail=$this->makeSecure($referralEmail);
		$referralAddress=$this->makeSecure($referralAddress);
		$referralCperson=$this->makeSecure($referralCperson);
		$referralCPhone=$this->makeSecure($referralCPhone);
		
		$query="UPDATE tbreferral_agent SET name='$referralName', commissionPercent='$referralCPercent', address='$referralAddress', phone='$referralPhone', email='$referralEmail', contactPersonName='$referralCperson', contactPhone='$referralCPhone' WHERE id='$id'";
  		return $this->insertData($query);
	}


	function deleteReferralAgentInfo($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbreferral_agent WHERE id = '$id'";
  		return $this->deleteData($query);
	}

	/* Users Area*/

	function getUserList($typeId){
		$typeId = $this->makeSecure($typeId);
  		$query ="SELECT * FROM tbusers WHERE userTypeId='$typeId' order by userFullName ASC";
  		return $this->getData($query);
	}

	function getUserListById($uid){
		$uid = $this->makeSecure($uid);
  		$query ="SELECT * FROM tbusers WHERE userid='$uid'";
  		return $this->getData($query);
	}

	function saveUserInformation($userFullName,$userTypeId,$userPhone,$userEmail,$userJoiningDate,$userAddress,$userName,$userPassword,$userImage){
		$userFullName = $this->makeSecure($userFullName);
		$userTypeId = $this->makeSecure($userTypeId);
		$userPhone = $this->makeSecure($userPhone);
		$userEmail = $this->makeSecure($userEmail);
		$userJoiningDate = $this->makeSecure($userJoiningDate);
		$userAddress = $this->makeSecure($userAddress);
		$userName = $this->makeSecure($userName);
		$userPassword = md5($this->makeSecure($userPassword));
		$userImage = $this->makeSecure($userImage);

  		$query ="INSERT INTO tbusers (userFullName, userTypeId, userPhone, userEmail, userJoiningDate, userAddress, userName, userPassword, userImage) VALUES('$userFullName','$userTypeId','$userPhone','$userEmail','$userJoiningDate','$userAddress','$userName','$userPassword','$userImage')";
  		return $this->insertData($query);
	}

	function updateUserInformation($id,$userFullName,$userPhone,$userEmail,$userJoiningDate,$userAddress,$userName,$userImage){
		$id = $this->makeSecure($id);
		$userFullName = $this->makeSecure($userFullName);
		$userTypeId = $this->makeSecure($userTypeId);
		$userPhone = $this->makeSecure($userPhone);
		$userEmail = $this->makeSecure($userEmail);
		$userJoiningDate = $this->makeSecure($userJoiningDate);
		$userAddress = $this->makeSecure($userAddress);
		$userName = $this->makeSecure($userName);
		$userImage = $this->makeSecure($userImage);

  		$query ="UPDATE tbusers SET userFullName='$userFullName', userPhone='$userPhone', userEmail='$userEmail', userJoiningDate='$userJoiningDate', userAddress='$userAddress', userName='$userName', userImage='$userImage' WHERE userid='$id'";
  		return $this->updateData($query);
	}

	function deleteUserInfo($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbusers WHERE userid = '$id'";
  		return $this->deleteData($query);
	}


	/* Test Category Management Area*/

	function getTestCategoryListWithCount(){
  		$query ="SELECT ttci.*,(SELECT COUNT(tti.testName) FROM tbtestinfo tti WHERE tti.testCategoryId = ttci.id) as totTestItem FROM tbtestcategoryinfo ttci order by ttci.testCategoryName ASC";
  		return $this->getData($query);
	}

	function getTestCategoryList(){
  		$query ="SELECT * FROM tbtestcategoryinfo order by testCategoryName ASC";
  		return $this->getData($query);
	}

	function getTestCategoryListById($id){
  		$query ="SELECT * FROM tbtestcategoryinfo WHERE id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	function saveTestCategoryInformation($testCategoryName,$testCategoryDescription){
		$testCategoryName = $this->makeSecure($testCategoryName);
		$testCategoryDescription = $this->makeSecure($testCategoryDescription);

  		$query ="INSERT INTO tbtestcategoryinfo (testCategoryName, testCategoryDescription) VALUES('$testCategoryName','$testCategoryDescription')";
  		return $this->insertData($query);
	}

	function updateTestCategoryInformation($id,$testCategoryName,$testCategoryDescription){
		$id = $this->makeSecure($id);
		$testCategoryName = $this->makeSecure($testCategoryName);
		$testCategoryDescription = $this->makeSecure($testCategoryDescription);

  		$query ="UPDATE tbtestcategoryinfo SET testCategoryName = '$testCategoryName', testCategoryDescription='$testCategoryDescription' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function deleteTestCategoryInfo($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbtestcategoryinfo WHERE id= '$id'";
  		return $this->deleteData($query);
	}


	/* Test Management Area*/

	function getTestList(){
  		$query ="SELECT ttci.testCategoryName,tti.* FROM tbtestinfo tti LEFT JOIN tbtestcategoryinfo ttci ON ttci.id=tti.testCategoryId ORDER BY testName ASC";
  		return $this->getData($query);
	}

	function getTestListByCatId($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT ttci.testCategoryName,tti.* FROM tbtestinfo tti LEFT JOIN tbtestcategoryinfo ttci ON ttci.id=tti.testCategoryId WHERE tti.testCategoryId='$id'";
  		return $this->getData($query);
	}

	function getTestListById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT ttci.testCategoryName,tti.* FROM tbtestinfo tti LEFT JOIN tbtestcategoryinfo ttci ON ttci.id=tti.testCategoryId WHERE tti.id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	function deleteTestInfo($id){
		$id = $this->makeSecure($id);
  		$query ="DELETE FROM tbtestinfo WHERE id= '$id'";
  		return $this->deleteData($query);
	}

	function saveTestInformation($testCategoryId,$testName,$testDescription,$testPrice,$testLocation){
		$testCategoryId = $this->makeSecure($testCategoryId);
		$testName = $this->makeSecure($testName);
		$testDescription = $this->makeSecure($testDescription);
		$testPrice = $this->makeSecure($testPrice);
		$testLocation = $this->makeSecure($testLocation);

  		$query ="INSERT INTO tbtestinfo (testCategoryId, testName, testDescription, testPrice, testLocation) VALUES('$testCategoryId','$testName','$testDescription','$testPrice','$testLocation')";
  		return $this->insertData($query);
	}

	function updateTestInformation($id,$testCategoryId,$testName,$testDescription,$testPrice,$testLocation){
		$id = $this->makeSecure($id);
		$testCategoryId = $this->makeSecure($testCategoryId);
		$testName = $this->makeSecure($testName);
		$testDescription = $this->makeSecure($testDescription);
		$testPrice = $this->makeSecure($testPrice);
		$testLocation = $this->makeSecure($testLocation);

  		$query ="UPDATE tbtestinfo SET testCategoryId='$testCategoryId', testName='$testName', testDescription='$testDescription', testPrice='$testPrice', testLocation='$testLocation' WHERE id='$id'";
  		return $this->updateData($query);
	}

	function checkTestById($id){
		$id = $this->makeSecure($id);
		$query="SELECT * FROM tbpatienttestdetails WHERE testId='$id'";
  		return $this->getDataWithValue($query);
	}

	function getAllTestListById($id){
		$id = $this->makeSecure($id);
		$query="SELECT tptd.*,tst.testName testName FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tst ON tptd.testId=tst.id WHERE tptd.testId='$id'";
  		return $this->getData($query);
	}

	function getTodaysAllAppointmentHistory(){
		date_default_timezone_set('Asia/Dhaka');
		$date=date("Y-m-d");

		$query="SELECT * FROM tbpatientappointment WHERE createDate='$date'";
  		return $this->getData($query);
	}

	function getTodaysAllTestHistory(){
		date_default_timezone_set('Asia/Dhaka');
		$date=date("Y-m-d");

		$query="SELECT tptd.*,tti.testName FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tti ON tti.id=tptd.testId WHERE tptd.createDate='$date'";
  		return $this->getData($query);
	}

	function getDateWiseReportAllAppointmentHistory($fromDate,$toDate){
		$fromDate = $this->makeSecure($fromDate);
		$toDate = $this->makeSecure($toDate);
		$query="SELECT * FROM tbpatientappointment WHERE createDate BETWEEN '$fromDate' AND '$toDate'";
  		return $this->getData($query);
	}

	function getDateWiseReportAllTestHistory($fromDate,$toDate){
		$fromDate = $this->makeSecure($fromDate);
		$toDate = $this->makeSecure($toDate);
		$query="SELECT tptd.*,tti.testName FROM tbpatienttestdetails tptd LEFT JOIN tbtestinfo tti ON tti.id=tptd.testId WHERE tptd.createDate BETWEEN '$fromDate' AND '$toDate'";
  		return $this->getData($query);
	}
}
?>
