<?php
 class MSBController{
 
	function connectDB(){
	 $conn = mysqli_connect("localhost","root","","dbmsb_hms");
	 mysqli_query($conn,'SET CHARACTER SET utf8'); 
	 mysqli_query($conn,"SET SESSION collation_connection ='utf8_general_ci'");
	return $conn;
	}

	function getData($query) {
		$result = mysqli_query($this->connectDB(),$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}		
		if(!empty($resultset)){
			return $resultset;
		}
	}	

	function getDataWithValue($query) {
		$result = mysqli_query($this->connectDB(),$query);
		while($row=mysqli_fetch_assoc($result)) {
			$resultset[] = $row;
		}	
		if(!empty($resultset)){
		$rownum=mysqli_num_rows($result);	
			return $rownum;
		}
	}	

	function getRowNumber($query) {
		$result = mysqli_query($this->connectDB(),$query);
		$rows=mysqli_num_rows($result);
		return $rows;
	}

	function insertData($query) {
		$result = mysqli_query($this->connectDB(),$query);
		if($result)
			return true;
	}


	function deleteData($query) {
		$result = mysqli_query($this->connectDB(),$query);
		if($result)
			return true;
	}


	function UpdateData($query) {
		$result = mysqli_query($this->connectDB(),$query);
		if($result)
			return true;
	}

	function makeSecure($value){
		$value=trim($value);
		$value = mysqli_escape_string($this->connectDB(),$value);
		return $value;
	}

}
	?>

