<?php
require_once("controller.php");

class myFunctions extends MSBController{
	
	/* All Global Function */

	function getTotalRowNumber($tbname){
		$tbname = $this->makeSecure($tbname);
  		$query ="SELECT * FROM $tbname ";
  		return $this->getRowNumber($query);
	}

	function checkDependency($tbname,$colname,$id){
		$tbname = $this->makeSecure($tbname);
		$colname = $this->makeSecure($colname);
		$id = $this->makeSecure($id);

	  	$query ="SELECT * FROM $tbname WHERE $colname ='$id'";
	  	return $this->getRowNumber($query);
	}
	
	/* Test Management Area*/

	function getTestList(){
  		$query ="SELECT * FROM tbtestinfo order by testName ASC";
  		return $this->getData($query);
	}

	function getTestDetails($id){
		$query ="SELECT * FROM tbtestinfo WHERE id='$id'";
  		return $this->getData($query);
	}

	function getTestListById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT * FROM tbtestinfo WHERE id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}


	/* Doctor Area */
	
	function getDoctorList(){
  		$query ="SELECT tdoc.*,td.departmentName departmentName FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId order by tdoc.name ASC";
  		return $this->getData($query);
	}

	function getDoctorShift($did){
		$query ="SELECT * FROM tbdoctorshift WHERE doctorId = '" . $did . "'";
  		return $this->getData($query);
	}


	function getDoctorTimeSchedule($did){
		date_default_timezone_set('Asia/Dhaka');
		$date=date("Y-m-d");
		$query ="SELECT tpa.*,tb.name patientName,tb.phone patientPhone FROM tbpatientappointment tpa LEFT JOIN tbpatient tb ON tb.id=tpa.patientId WHERE apDate >='$date' && doctorId = '$did' order by apDate asc";
  		return $this->getData($query);
	}

	function getDoctorInformationById($id){
		$id = $this->makeSecure($id);
  		$query ="SELECT tdoc.*,td.* FROM tbdoctors tdoc LEFT JOIN tbdepartment td ON td.id=tdoc.docDepartmentId WHERE tdoc.id='$id' LIMIT 0,1";
  		return $this->getData($query);
	}

	// ************************** Patient Testing Area ************************************

	function getPatientAppoinmentDetails($id){
		$query ="SELECT pa.*, d.name doctorname FROM tbpatientappointment pa INNER JOIN tbdoctor d ON pa.doctorId=d.id where pa.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestList($id){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientTestDetailsById($tid,$pid){
		$query ="SELECT pb.*,tst.testName testName FROM tbpatienttestdetails pb LEFT JOIN tbtestinfo tst ON pb.testId=tst.id where pb.id='$tid' && pb.patientId='$pid'";
  		return $this->getData($query);
	}

	function getPatientPayment($pid){
		$query ="SELECT * FROM tbpatientpayment where patientId='$pid'";
  		return $this->getData($query);
	}

	function checkPatientId($pid){
		$query="SELECT * FROM tbpatient where id='$pid'";
  		return $this->getDataWithValue($query);
	}



	// ************************** Patient Area ************************************

	function getPatientDetails($id){
		$query ="SELECT * FROM tbpatient where id='$id'";
  		return $this->getData($query);
	}
	
	function getPatientMedicalHistory($id){
		$query ="SELECT * FROM tbmedicalhistory where patientId='$id'";
  		return $this->getData($query);
	}

	function getPatientList(){
		$query ="SELECT * FROM tbpatient order by addDate DESC";
  		return $this->getData($query);
	}

	function getPatientListForAutocomplete($id){
		$query ="SELECT DISTINCT id FROM tbpatient where id LIKE '%$id%'";
  		return $this->getData($query);
	}


	/* Users Area*/

	function getUserList($typeId){
		$typeId = $this->makeSecure($typeId);
  		$query ="SELECT * FROM tbusers WHERE userTypeId='$typeId' order by userFullName ASC";
  		return $this->getData($query);
	}

	function getUserListById($uid){
		$uid = $this->makeSecure($uid);
  		$query ="SELECT * FROM tbusers WHERE userid='$uid'";
  		return $this->getData($query);
	}


	function getPatientMedicineDate($pid){
		$query ="SELECT distinct pdate FROM tbpatientmedicine WHERE patientId ='$pid' order by pdate desc";
		return $this->getData($query);
	}

		
	function savePMedicine($pid,$medicine,$uses,$day,$instruction){
		$pid=$this->makeSecure($pid);
		$medicine=$this->makeSecure($medicine);
		$uses=$this->makeSecure($uses);
		$day=$this->makeSecure($day);
		$instruction=$this->makeSecure($instruction);
		date_default_timezone_set('Asia/Dhaka');
		$date=date("Y-m-d");
		
		$query="INSERT INTO tbpatientmedicine (patientId,medicine,uses,days,instruction,pdate) VALUES('$pid','$medicine','$uses','$day','$instruction','$date')";
  		return $this->insertData($query);
	}

	function getPatientMedicine($pid,$pdate){
		$query ="SELECT tpm.* FROM tbpatientmedicine tpm WHERE tpm.patientId ='$pid' && tpm.pdate='$pdate'";
		return $this->getData($query);
	}

	function getDoctorShiftById($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.doctorId='$id'";
  		return $this->getData($query);
	}

	function getMedicineForAutocomplete($id){
  		$query ="SELECT * FROM tbmedicine WHERE name LIKE '%$id%'";
  		return $this->getData($query);
	}
	
	function getDoctorShiftByShiftId($id){
  		$query ="SELECT tdoc.name doctorName,tds.* FROM tbdoctorshift tds LEFT JOIN tbdoctors tdoc ON tds.doctorId=tdoc.id WHERE tds.id='$id'";
  		return $this->getData($query);
	}

	function uploadTestAttachment($patientId,$testId,$testAttachment,$labAttendantId){
		$patientId = $this->makeSecure($patientId);
		$testId = $this->makeSecure($testId);
		$testAttachment = $this->makeSecure($testAttachment);
		$labAttendantId = $this->makeSecure($labAttendantId);
  		$query ="UPDATE tbpatienttestdetails SET testAttachment='$testAttachment',labAttendantId='$labAttendantId' WHERE id='$testId'&&patientId='$patientId'";
  		return $this->updateData($query);
	}
}
?>
