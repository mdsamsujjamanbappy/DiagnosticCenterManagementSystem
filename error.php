<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>IMS | Demo Shop</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="assets/css/bootstrap-progressbar.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">


    <?php 
      require_once("assets/tools/login.php"); 
      $db_handle = new myLoginFunctions();                          

      if(isset($_POST['uname'])&&isset($_POST['upass'])){
           $uname = ($_POST['uname']);
           $upass = ($_POST['upass']);

           $r = $db_handle->checkMSBLoginDetails($uname,$upass);

           if($r){ 


              $results = $db_handle->usersDetailsAtLogin($uname,$upass);
              foreach($results as $user) {
                  $id=htmlentities($user["userid"]); 
                  $userFullName=htmlentities($user["userFullName"]); 
                  $type=htmlentities($user["userTypeId"]); 
              }

              if($type==1){
                  $_SESSION['adAccess']="Permit";
                  $_SESSION['adId']=$id;
                  $_SESSION['fname']=$userFullName; 
                  echo "<script> document.location.href='admin/dashboard.php';</script>";
                  }
             
           }else{ ?>
             
         <?php }

          }
          ?>

        <div class="animate form login_form">
          <section class="login_content">
            <form action="" method="POST">
              <h1>Login Area</h1>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="uname" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="upass" required="" />
              </div>
              <div>
                <center><input type="submit" class="btn btn-default" value="Log in" /></center>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <a class="signup"  href="#signup" >Lost your password?</a>

                <br />
                <br />
                <br />

                <div>
                    Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <form action="#" method="POST">
              <center>
              <h1>Recover Account</h1>
              <div>
                <input type="text" required class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" required class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="submit" class="btn btn-default" value="Submit" />
              </div>
              </center>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
