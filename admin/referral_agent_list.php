<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>All Referral Agent List</title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-users"></i> All Referral Agent List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="add_new_referral_agent_info.php"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Referral Agent</button></a>
                       </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?php if(isset($_GET['ssuccess'])){ 
                           echo "<h2 style='color:green;'>Referral agent has been successfully inserted.</h2>";

                            echo "<hr >";
                      } ?>
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                          <th width="8%">Serial</th>
                          <th>Referral Agency Name</th>
                          <th>Phone</th>
                          <th><center>Com. Percent</center></th>
                          <th width="20%">Action</th>
                      </thead>
                      <tbody>
                      <?php
                        $results = $db_handle->getReferralAgentList();
                        $i=0;
                        $trow=count($results);
                        if($trow>0){
                         foreach($results as $dataArr) {
                      ?>
                      <tr>
                          <td><?php echo ++$i; ?></td>
                          <td><?php echo ($dataArr["name"]); ?></td>
                          <td><?php echo ($dataArr["phone"]); ?></td>
                          <td align="center"><?php echo ($dataArr["commissionPercent"])."%"; ?></td>
                          <td>
                              <a  href="details_referral_agent_info.php?iid=<?php echo md5('MSBCSE');?>&&id=<?php echo base64_encode($dataArr['id']); ?>" class="btn btn-xs btn-primary"> <i class="fa fa-chrome"></i> Details</a>
                              <a  href="edit_referral_agent_info.php?iid=<?php echo md5('MSBCSE');?>&&id=<?php echo base64_encode($dataArr['id']); ?>" class="btn btn-xs btn-success"> <i class="fa fa-edit"></i> Edit</a>
                              <a onClick="return confirm('Do you want to delete this Referral Agent Information?');"  class="btn btn-xs btn-danger"  href="delete_referral_info.php?iid=<?php echo md5('MSBCSE');?>&&id=<?php echo base64_encode($dataArr['id']); ?>"><i class="fa fa-trash"></i> Delete</a>
                          </td>
                      </tr>
                      <?php 
                      } 
                      }else{
                        echo "<tr><td colspan='6' > <center>No data are matching</center></td></tr>";
                      } 
                      ?>
                      </tbody>
                    </table>
          
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>