<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>All Department List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a href="add_department.php"><button class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add New Department</button></a>
                       </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                          <th width="8%">Serial</th>
                          <th>Department Name</th>
                          <th>Department Description</th>
                          <th width="15%">Action</th>
                     </thead>
                     <tbody>
                      <?php
                       $results = $db_handle->getDepartment();

                      $i=0;
                      $trow=count($results);
                      if($trow>0){
                       foreach($results as $Department) {

                      ?>
                     <tr>
                          <td><?php echo ++$i; ?></td>
                          <td><?php echo ($Department["departmentName"]); ?></td>
                          <td><?php echo ($Department["departmentDescription"]); ?></td>
                          <td>
                              <a rel="facebox" href="edit_department.php?iid=<?php echo md5('MSBIT');?>&&id=<?php echo base64_encode($Department['id']); ?>" class="btn btn-xs btn-success">Edit Info</a>
                              <a onClick="return confirm('Do you want to Delete this Department?');"  class="btn btn-xs btn-danger"  href="delete_department.php?iid=<?php echo md5('MSBIT');?>&&id=<?php echo base64_encode($Department['id']); ?>">Delete</a>
                          </td>
                     </tr>
                     <?php 
                       
                      } 
                     }else{
                        echo "<tr><td colspan='4' > <center>No data are matching</center></td></tr>";
                      } 
                      ?>
                      </tbody>
                  </table>
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>