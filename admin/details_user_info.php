<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>User Details</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <h2>User Details</h2>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              
                             <div class="" role="tabpanel" data-example-id="togglable-tabs" >
                      <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist" >
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Personal Information</a>
                        </li>
                      </ul>
                      <div id="myTabContent" class="tab-content">
                       
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                           
                        <div class="row">
                          <div class="col-md-12 col-md-12">
                            <table width="450px;" class="table table-bordered table-hover table-striped" >
                             <?php 
                              $id = base64_decode($_REQUEST['id']);
                              $results = $db_handle->getUserListById($id);
                              if(count($results)>0){
                                  foreach($results as $dataArr) {
                              ?>
                              <tr><td width="25%">Fullname:</td><td><b><?php echo $doctorName=htmlentities($dataArr["userFullName"]); ?></b></td></tr>
                              <tr><td>Type:</td><td><b>
                              <?php if($_GET[]){

                                } ?>
                              </b></td></tr>
                              <tr><td>Designation:</td><td><b><?php echo htmlentities($dataArr["designation"]); ?></b></td></tr>
                              <tr><td>Qualification:</td><td><b><?php echo htmlentities($dataArr["qualification"]); ?></b></td></tr>
                              <tr><td>Phone:</td><td><b><?php echo htmlentities($dataArr["phone"]); ?></b></td></tr>
                              <tr><td>Email/Username:</td><td><b><?php echo htmlentities($dataArr["email"]); ?></b></td></tr>
                              <tr><td>Allocated Room:</td><td><b><?php echo htmlentities($dataArr["hospitalDetails"]); ?></b></td></tr>
                              <tr><td>Address:</td><td><?php echo htmlentities($dataArr["address"]); ?></td></tr>
                              <tr><td>Others Information:</td><td><?php echo htmlentities($DoctorInformation["othersInfo"]); ?></td></tr>
                              <tr><td>Registration Number:</td><td><?php echo htmlentities($DoctorInformation["registrationNumber"]); ?></td></tr>
                              <tr><td>Photo:</td><td><img class="img img-thumbnail" width="150px" src="../doctor_images/<?php echo ($DoctorInformation["image"]); ?>" alt="Not Found." ></b></td></tr>
                            <?php }} ?>
                            </table>
                          </div>
                        </div>

                        </div>

                      </div>
                    </div>

                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

  </body>
</html>