<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Dashboard</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <div class="row">
                      <div class="col-md-12">
                         
                         <div class="row top_tiles">
                            
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-user-md"></i></div>
                                <div class="count"><?php echo count($db_handle->getDoctorList());?></div>
                                <h3 style="color:#484848;font-weight:bold;">Doctors</h3><br>
                                <p><a href="doctor_list.php" class="btn btn-success btn-sm"> <i class="fa fa-share-square "></i> View Doctor List</a></p>
                              </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-drupal"></i></div>
                                <div class="count"><?php echo count($db_handle->getUserList('3'));?></div>
                                <h3 style="color:#484848;font-weight:bold;">Receptionists</h3><br>
                                <p><a href="receptionist_list.php" class="btn btn-info btn-sm"> <i class="fa fa-share-square "></i> View Receptionists List</a></p>
                              </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i></div>
                                <div class="count"><?php echo count($db_handle->getUserList('5'));?></div>
                                <h3 style="color:#484848;font-weight:bold;">LAB Attendant</h3><br>
                                <p><a href="lab_attendant_list.php" class="btn btn-primary btn-sm"> <i class="fa fa-share-square"></i> LAB Attendant List</a></p>
                              </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-bed"></i></div>
                                <div class="count"><?php echo count($db_handle->getPatientList());?></div>
                                <h3 style="color:#484848;font-weight:bold;">Patients</h3><br>
                                <p><a href="patient_list.php" class="btn btn-success btn-sm"> <i class="fa fa-share-square"></i> View Patients List</a></p>
                              </div>
                            </div>
                            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                              <div class="tile-stats">
                                <div class="icon"><i class="fa fa-users"></i></div>
                                <div class="count"><?php echo count($db_handle->getReferralAgentList());?></div><br>
                                <h3 style="color:#484848;font-weight:bold;">Referral Agent</h3><br>
                                <p><a href="referral_agent_list.php" class="btn btn-info btn-sm"> <i class="fa fa-share-square"></i> View Referral Agent List</a></p>
                              </div>
                            </div>

                          </div>
                         
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
  </body>
</html>