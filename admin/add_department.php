<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add New Department</title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <span style="color:black !important;font-weight: bold;font-size: 16px;">Add New Department</span>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              
                             <center>
                               <?php if (isset($_POST['save'])) {
                                  if(isset($_POST['_MSBtoken'])){

                                $name = ($_POST['name']);
                                $description = ($_POST['description']);
                                $r = $db_handle->saveDepartment($name,$description);

                                if($r==1){
                                    echo "<h2 style='color:green;'>Department has been Successfully Inserted.</h2>";

                                    echo "<br />";

                                    echo "<a href='add_department.php' style='margin-bottom:8px; margin-top:-5px;' class='btn btn-sm btn-success'>Add Another Department</a> &nbsp;&nbsp;";

                                    echo "<a href='department_list.php' style='margin-bottom:8px; margin-top:-5px;' class='btn btn-sm btn-primary'>Manage Department</a>";
                                }else{
                                    echo "<h2 style='color:red;'>Insertion Failed!!</h2>";

                                    echo "<br />";
                                    echo "<br />";

                                    echo "<a href='add_department.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Try Again</a> &nbsp;&nbsp;";
                                  }
                                }else{
                                    echo "Invalid Token";
                                }
                               }else{ ?>
                                   <form action="" method="POST">
                                        <table width="500px;">
                                          <tr><td>Department Name:</td><td>
                                          <input name="name" autofocus style="margin-top:10px;" class="form-control" required type="text" placeholder="Insert department name ...">
                                          <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>"></td></tr>
                                          
                                          <tr> <td> Department Description:</td><td> <textarea name="description"  style="margin-top:10px;" class="form-control" placeholder="Insert department description ..."></textarea></td></tr>
                                          
                                          <tr><td></td><td><input style="margin-top:25px;" class="btn btn-success"  name="save" type="submit" value="Save Department" ><a href="department_list.php" style="margin-top:25px;" class="btn btn-primary"><i class="fa fa-refresh"></i> Go Back</a></td></tr>
                                        </table>
                                          
                                    </form>
                                    <?php } ?>
                                </center>

                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>