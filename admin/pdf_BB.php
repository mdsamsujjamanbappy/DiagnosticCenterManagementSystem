<?php
ob_start();
require_once 'header_link.php';
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this->SetX(5);
		$this->SetTextColor(254, 71, 71);
		$this ->SetFont('Arial','B',20);;
		$this->Cell(198,8,invoiceCompanyTitle(),0,0,'R');
		$this->Ln(8);
		
		$this->SetTextColor(0, 0, 0);
		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyAddress(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyPhone(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyEmail(),0,0,'R');
		$this->Ln(9);

		// Header line
		$this->SetX(35);
		$this->SetAlpha(0.9);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,42,202,42);
		$this->Ln(9);
		
		// Header Image
		$this->SetAlpha(0.2);
		$image="headerimg.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,0,0,220,15);

		// Logo Image
		$this->SetAlpha(1);
		$image="logo.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,10,10,35,26);

		// Rx Image
		$this->SetAlpha(0.2);
		$image="rx.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,12,60,15,15);

		// Background Watermark Image
		$this->SetAlpha(0.07);
		$image="img2.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,35,75,130,130);
	}

	function Footer()
	{

		// Footer line
	    $this->SetY(-18);
		$this->SetAlpha(1);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,280,202,280);
		$this->Ln(9);

	    $this->SetY(-15);
		$this->SetAlpha(0.6);
	    $this->SetFont('Helvetica','I',12);
	    $this->Cell(180,10,'*** Smoking is injurious to health ***',0,0,'C');
	    

		$this->SetAlpha(0.2);
		$image="footer.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,0,280,220,0);
	}

	var $extgstates = array();

    function SetAlpha($alpha, $bm='Normal')
    {
        // set alpha for stroking (CA) and non-stroking (ca) operations
        $gs = $this->AddExtGState(array('ca'=>$alpha, 'CA'=>$alpha, 'BM'=>'/'.$bm));
        $this->SetExtGState($gs);
    }

    function AddExtGState($parms)
    {
        $n = count($this->extgstates)+1;
        $this->extgstates[$n]['parms'] = $parms;
        return $n;
    }

    function SetExtGState($gs)
    {
        $this->_out(sprintf('/GS%d gs', $gs));
    }

    function _enddoc()
    {
        if(!empty($this->extgstates) && $this->PDFVersion<'1.4')
            $this->PDFVersion='1.4';
        parent::_enddoc();
    }

    function _putextgstates()
    {
        for ($i = 1; $i <= count($this->extgstates); $i++)
        {
            $this->_newobj();
            $this->extgstates[$i]['n'] = $this->n;
            $this->_out('<</Type /ExtGState');
            $parms = $this->extgstates[$i]['parms'];
            $this->_out(sprintf('/ca %.3F', $parms['ca']));
            $this->_out(sprintf('/CA %.3F', $parms['CA']));
            $this->_out('/BM '.$parms['BM']);
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    function _putresourcedict()
    {
        parent::_putresourcedict();
        $this->_out('/ExtGState <<');
        foreach($this->extgstates as $k=>$extgstate)
            $this->_out('/GS'.$k.' '.$extgstate['n'].' 0 R');
        $this->_out('>>');
    }

    function _putresources()
    {
        $this->_putextgstates();
        parent::_putresources();
    }
}

$pdf = new PDF('P','mm','A4');
$pdf->AddPage();
$pdf->AliasNbPages();

$pdf->SetAlpha(1);

$filename = "Invoice".".pdf";

$pdf->Output("",$filename,"false");

ob_end_flush();
?>
