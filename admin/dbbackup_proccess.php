<?php
backup_tables("localhost","root","","dbmsb_hms");

/* backup the db OR just a table */
function backup_tables($host,$user,$pass,$name,$tables = '*')
{
    $return="";

    $link = mysqli_connect($host,$user,$pass);
    mysqli_select_db($link,$name);

     mysqli_query($link,'SET CHARACTER SET utf8'); 
     mysqli_query($link,"SET SESSION collation_connection ='utf8_general_ci'");
    //get all of the tables
    if($tables == '*')
    {
        $tables = array();
        $result = mysqli_query($link,'SHOW TABLES');
        while($row = mysqli_fetch_row($result))
        {
            $tables[] = $row[0];
        }
    }
    else
    {
        $tables = is_array($tables) ? $tables : explode(',',$tables);
    }

    //cycle through
    foreach($tables as $table)
    {
        $result = mysqli_query($link,'SELECT * FROM '.$table);
        $num_fields = mysqli_num_fields($result);

        $return.= 'DROP TABLE '.$table.';';
        $row2 = mysqli_fetch_row(mysqli_query($link,'SHOW CREATE TABLE '.$table));
        $return.= "\n\n".$row2[1].";\n\n";

        for ($i = 0; $i < $num_fields; $i++)
        {
            while($row = mysqli_fetch_row($result))
            {
                $return.= 'INSERT INTO '.$table.' VALUES(';
                for($j=0; $j < $num_fields; $j++)
                {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = mb_ereg_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j < ($num_fields-1)) { $return.= ','; }
                }
                $return.= ");\n";
            }
        }
        $return.="\n\n\n";
    }

    $url = '../dbbackup/dbbackup-'.date("d-m-Y_H").'.sql';
    //save file
    $handle = fopen($url,'w+');
    $complete = fwrite($handle,$return);
    if($complete){
        sleep(5);
        header("location:$url");
        echo "<script>alert('Database backup is completed successfully!');</script>";
        echo "<script> document.location.href='dbbackup.php';</script>";
    }
    fclose($handle);
}

