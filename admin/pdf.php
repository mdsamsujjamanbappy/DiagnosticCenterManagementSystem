<?php
ob_start();
require_once 'header_link.php';
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this->SetX(5);
		$this->SetTextColor(0, 0, 0);
		$this ->SetFont('Helvetica','B',20);;
		$this->Cell(198,8,"Remedy Physiotherapy Hospital",0,0,'R');
		$this->Ln(8);
		
		$this->SetTextColor(0, 0, 0);
		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyAddress(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(40,10,"",0,0,'L');
		$this->Cell(58,10,"",0,0,'L');
		$this->Cell(100,10,invoiceCompanyPhone(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyEmail(),0,0,'R');
		$this->Ln(9);

		// Header line
		$this->SetX(35);
		$this->SetAlpha(0.9);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,42,202,42);
		$this->Ln(4);

		// Logo Image
		$this->SetAlpha(1);
		$image="logo1.jpeg";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,15,5,35,35);

		// Rx Image
		$this->SetAlpha(0.3);
		$image="rx.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,12,62,15,15);

		// Background Watermark Image
		$this->SetAlpha(0.09);
		$image="img2.png";
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,35,75,130,130);
	}

	function Footer()
	{

		// Footer line
	    $this->SetY(-18);
		$this->SetAlpha(1);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,280,202,280);
		$this->Ln(9);

	    $this->SetY(-15);
		$this->SetAlpha(0.6);
	    $this->SetFont('Helvetica','I',12);
	    $this->Cell(90,10,'*** Slogan goes here ***',0,0,'L');
	    $this->Cell(100,10,'Another text goes here ',0,0,'R');
	    
	}

	var $extgstates = array();

    function SetAlpha($alpha, $bm='Normal')
    {
        // set alpha for stroking (CA) and non-stroking (ca) operations
        $gs = $this->AddExtGState(array('ca'=>$alpha, 'CA'=>$alpha, 'BM'=>'/'.$bm));
        $this->SetExtGState($gs);
    }

    function AddExtGState($parms)
    {
        $n = count($this->extgstates)+1;
        $this->extgstates[$n]['parms'] = $parms;
        return $n;
    }

    function SetExtGState($gs)
    {
        $this->_out(sprintf('/GS%d gs', $gs));
    }

    function _enddoc()
    {
        if(!empty($this->extgstates) && $this->PDFVersion<'1.4')
            $this->PDFVersion='1.4';
        parent::_enddoc();
    }

    function _putextgstates()
    {
        for ($i = 1; $i <= count($this->extgstates); $i++)
        {
            $this->_newobj();
            $this->extgstates[$i]['n'] = $this->n;
            $this->_out('<</Type /ExtGState');
            $parms = $this->extgstates[$i]['parms'];
            $this->_out(sprintf('/ca %.3F', $parms['ca']));
            $this->_out(sprintf('/CA %.3F', $parms['CA']));
            $this->_out('/BM '.$parms['BM']);
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    function _putresourcedict()
    {
        parent::_putresourcedict();
        $this->_out('/ExtGState <<');
        foreach($this->extgstates as $k=>$extgstate)
            $this->_out('/GS'.$k.' '.$extgstate['n'].' 0 R');
        $this->_out('>>');
    }

    function _putresources()
    {
        $this->_putextgstates();
        parent::_putresources();
    }
}

$pdf = new PDF();
$pdf->AddPage();

$pdf->SetAlpha(1);
$pdf->SetFont('Arial','',10);;
$pdf->Cell(19,10,"Patient ID: ",0,0,'L');
$pdf->SetFont('Arial','B',11);;
$pdf->Cell(85,10,"20180205044921",0,0,'L');
$pdf->SetFont('Arial','',11);;
$pdf->Cell(25,10,"Doctor Name: ",0,0,'L');
$pdf->SetFont('Arial','B',10);;
$pdf->Cell(68,10,"Dr. Maruf Alam Chowdhury",0,0,'L');
$pdf->Ln(7);

$pdf->SetAlpha(1);
$pdf->SetFont('Arial','',11);;
$pdf->Cell(26,10,"Patient Name: ",0,0,'L');
$pdf->SetFont('Arial','B',10);;
$pdf->Cell(80,10,"Md. Samsujjaman Bappy",0,0,'L');
$pdf->SetFont('Arial','',11);;
$pdf->Cell(9,10,"Age: ",0,0,'L');
$pdf->SetFont('Arial','B',11);;
$pdf->Cell(28,10,"23 y",0,0,'L');
$pdf->SetFont('Arial','',11);;
$pdf->Cell(13,10,"Date: ",0,0,'R');
$pdf->SetFont('Arial','B',11);;
$pdf->Cell(25,10,"05-02-2018",0,0,'L');
$pdf->Ln(10);



$filename = "Invoice".".pdf";

$pdf->Output("",$filename,"false");

ob_end_flush();
?>
