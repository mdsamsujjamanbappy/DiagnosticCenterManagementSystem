<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add New Doctor</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <span style="color:black !important;font-weight: bold;font-size: 16px;">Add New Doctor</span>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              
                             <center>
                               <?php 
                               if (isset($_POST['saveDoctor'])) {
                                  if(isset($_POST['_MSBtoken'])){

                                $name = ($_POST['name']);
                                $docDepartmentId = ($_POST['docDepartmentId']);
                                $designation = ($_POST['designation']);
                                $qualification = ($_POST['qualification']);
                                $phone = ($_POST['phone']);
                                $email = ($_POST['email']);
                                $password = ($_POST['password']);
                                $allocatedRoom = ($_POST['allocatedRoom']);
                                $address = ($_POST['address']);
                                $otherInfo = ($_POST['otherInfo']);
                                $registrationNumber = ($_POST['registrationNumber']);
                                $doctorImage = "ddefault.jpg";

                                if(isset($_FILES['doctorImage']['name'])){
                                    if(($_FILES['doctorImage']['size']) <= (3000*1024))
                                    {
                                          $ddata = date("Ymdhsi");
                                           $picture_tmp = $_FILES['doctorImage']['tmp_name'];
                                            $picture_name = $_FILES['doctorImage']['name'];
                                            $picture_type = $_FILES['doctorImage']['type'];
                                           
                                            $arr1 = explode(".", $picture_tmp);
                                            $extension1 = strtolower(array_pop($arr1));  
                                            
                                            $arr = explode(".", $picture_name);
                                            $extension = strtolower(array_pop($arr));
              
                                            $doctorImage="doctor".$ddata."".$phone .".".$extension;
                                            $newfilename1="doctor".$ddata."".$phone .".".$extension1;
                                            $path = '../doctor_images/'.$doctorImage;

                                            move_uploaded_file($picture_tmp, $path);
                                    }
                                  }

                                $r = $db_handle->saveDoctorInformation($name,$docDepartmentId,$designation,$qualification,$phone,$email,$password,$allocatedRoom,$address,$otherInfo,$registrationNumber,$doctorImage);

                                if($r){
                                      echo "<script> document.location.href='doctors_confirmation.php?ssuccess=success';</script>";
                                   
                                }else{
                                    echo "<h2 style='color:red;'>Insertion Failed!!</h2>";

                                    echo "<br />";
                                    echo "<br />";

                                    echo "<a href='add_doctor.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Try Again</a> &nbsp;&nbsp;";
                                  }
                                }else{
                                    echo "Invalid Token";
                                }
                               }else{ ?>
                                   <form action="" method="POST" class="form-horizontal form-label-left"  enctype="multipart/form-data" >

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Doctor's Name: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input name="name" autofocus class="form-control" required type="text" placeholder="Insert doctor's name here ...">
                                          <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Department">Department: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                         <select class="form-control" name="docDepartmentId" required autofocus >
                                                <option value=""> Select Doctor's Department</option>
                                                  <?php
                                                    $results = $db_handle->getDepartment();
                                                 foreach($results as $department) {
                                                  ?>
                                                     <option value="<?php echo $department['id']; ?>"><?php echo $department['departmentName']; ?></option>
                                                  <?php } ?>
                                          </select>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Doctor's Designation:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input name="designation" class="form-control" type="text" placeholder="Insert doctor's designation here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="qualification">Doctor's Qualification:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="qualification" name="qualification" class="form-control" type="text" placeholder="Insert doctor's qualification here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Doctor's Phone: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="phone" name="phone" required class="form-control" type="text" placeholder="Insert doctor's phone number here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Doctor's Email/Username: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="email" name="email" autofocus class="form-control" required type="text" placeholder="Insert doctor's email address/ username here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="password">Doctor's Password: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="password" name="password" autofocus class="form-control" required type="password" placeholder="Insert doctor's password here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="allocatedRoom">Allocated Room: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="allocatedRoom" name="allocatedRoom"  class="form-control" required type="text" placeholder="Insert allocated room for doctor here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address">Doctor's Address:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <textarea id="address" name="address" class="form-control" placeholder="Insert doctor's address here ..."></textarea>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="otherInfo">Doctor's Other Info:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <textarea id="otherInfo" name="otherInfo" class="form-control" placeholder="Insert doctor's other information here ..."></textarea>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="registrationNumber">Registration Number:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="registrationNumber" name="registrationNumber" class="form-control"  type="text" placeholder="Insert doctor's registration number here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="doctorImage">Doctor's Image:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="doctorImage" name="doctorImage"  class="form-control btn btn-info" type="file" >
                                        </div>
                                      </div>

                                      <div class="ln_solid"></div>

                                      <div class="form-group" style='margin-top:30px;'>
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-success"  name="saveDoctor" type="submit" ><i class="fa fa-save "></i> Save Doctor Information</button>
                                        <a href="doctor_list.php" class="btn btn-primary"><i class="fa fa-refresh "></i> Go Back</a>
                                        </div>
                                      </div>
                                          
                                    </form>
                                    <?php } ?>
                                </center>

                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

  </body>
</html>