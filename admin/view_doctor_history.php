<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Doctor History</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                            <?php 
                             $id = base64_decode($_REQUEST['did']);
                             $results = $db_handle->getDoctorInformationById($id);
                             if(count($results)>0){ ?>
                               
                                <div id="div_print">

                                <div class="row" style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                                  <div class="col-md-8 col-sm-12 col-xs-12" >
                                    <table width="450px;" class="table table-bordered table-hover table-striped" >
                               <?php 
                                $results = $db_handle->getDoctorInformationById($id);
                                if(count($results)>0){
                                    foreach($results as $DoctorInformation) {
                                ?>
                                <tr><td width="25%">Doctor Name:</td><td><b><?php echo $doctorName=htmlentities($DoctorInformation["name"]); ?></b></td></tr>
                                <tr><td>Department:</td><td><b><?php echo htmlentities($DoctorInformation["departmentName"]); ?></b></td></tr>
                                <tr><td>Designation:</td><td><b><?php echo htmlentities($DoctorInformation["designation"]); ?></b></td></tr>
                                <tr><td>Qualification:</td><td><b><?php echo htmlentities($DoctorInformation["qualification"]); ?></b></td></tr>
                                <tr><td>Phone:</td><td><b><?php echo htmlentities($DoctorInformation["phone"]); ?></b></td></tr>
                                <tr><td>Email/Username:</td><td><b><?php echo htmlentities($DoctorInformation["email"]); ?></b></td></tr>
                                <tr><td>Allocated Room:</td><td><b><?php echo htmlentities($DoctorInformation["hospitalDetails"]); ?></b></td></tr>
                                <tr><td>Address:</td><td><?php echo htmlentities($DoctorInformation["address"]); ?></td></tr>
                                <tr><td>Others Information:</td><td><?php echo htmlentities($DoctorInformation["othersInfo"]); ?></td></tr>
                                <tr><td>Registration Number:</td><td><?php echo htmlentities($DoctorInformation["registrationNumber"]); ?></td></tr>

                              <?php $dimg = $DoctorInformation["image"]; }} ?>
                              </table>

                                </div>
                                <div class="col-md-4 col-sm-12 col-xs-12 no-print" >
                                <center><img class="img img-thumbnail" width="200px" src="../doctor_images/<?php echo ($dimg); ?>" alt="Not Found." ></b></center>
                                   <br>
                                   <br>
                              <table class="table table-bordered table-hover table-striped" >
                              <tr>
                                <td colspan="3"><h5><b><center>Doctor's Visiting Shift</center></b></h5></td>
                              </tr> 
                              <?php 
                                $i=0;
                                $results = $db_handle->getDoctorShiftById($id);
                                if(count($results)>0){
                                  ?>
                                <tr>
                                  <th width="7%">Serial</th>
                                  <th>Day</th>
                                  <th>Shift/Visiting Time</th>
                                </tr>
                                  <?php 
                                    foreach($results as $DoctorInformation) {
                                  ?>
                              <tr>
                                <td><?php echo ++$i; ?></td>
                                <td><?php echo htmlentities($DoctorInformation["day"]); ?></td>
                                <td><?php echo htmlentities($DoctorInformation["time"]); ?></td>
                                

                              </tr>
                               <?php }}else{ echo "<h5 style='padding-top:25px;'>No Data Found.</h5>";} ?>
                              </table>

                                </div>
                                </div>
                                <hr>
                                <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #c5c5c5;padding:12px;">
                                  <table id="datatable" class="table table-striped table-bordered table-responsive ">
                                  <thead>
                                     <tr>
                                        <th width="4%">SN</th>
                                        <th width="14%">Patient ID</th>
                                        <th width="14%">Patient Name</th>
                                        <th width="8%">Consultation Date</th>
                                        <th width="10%">Consultation Time</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                     <?php 
                                     $results1 = $db_handle->getPatientAppointmentListByDoctorId($id);
                                     $i=0;
                                     if(count($results1)>0){
                                        foreach($results1 as $dataArr) {
                                      ?> 
                                      <tr>
                                        <td><?php echo ++$i; ?></td>
                                        <td><?php echo $dataArr['patientId']; ?></td>
                                        <td><?php echo $dataArr['patientName']; ?></td>
                                        <td><?php echo date('d-m-Y', strtotime($dataArr['apDate'])); ?></td>
                                        <td><?php echo $dataArr['apTime']; ?></td>
                                      </tr>
                                      <?php }
                                      } ?>
                                      </tbody>
                                      </table>

                                  </div>
                                </div>
                                <br > <br >

                                </div>

                               <?php }else{ ?>
                                    <h1 style='color:red;' ><strong>Invalid Doctor ID !</strong></h1><br /><br />
                               <?php } ?>

          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>