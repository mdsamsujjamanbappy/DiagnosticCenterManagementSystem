<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><i class="fa fa-edit"></i> Edit Test Information</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                   <center>
                    <?php 
                     if (isset($_POST['saveInfo'])) {
                        if(isset($_POST['_MSBtoken'])){

                      $id = ($_POST['id']);
                      $testCategoryId = ($_POST['testCategoryId']);
                      $testName = ($_POST['testName']);
                      $testDescription = ($_POST['testDescription']);
                      $testPrice = ($_POST['testPrice']);
                      $testLocation = ($_POST['testLocation']);

                      $r = $db_handle->updateTestInformation($id,$testCategoryId,$testName,$testDescription,$testPrice,$testLocation);

                      if($r){
                            echo "<script>document.location.href='test_confirmation.php?usuccess=success';</script>";
                         
                      }else{
                          echo "<h2 style='color:red;'>Insertion Failed!!</h2>";

                        }
                      }else{
                          echo "Invalid Token";
                      }
                     }else{ ?>
                        <?php
                         $id = base64_decode($_GET['id']);
                          $results = $db_handle->getTestListById($id);

                          $i=0;
                          $trow=count($results);
                          if($trow>0){
                           foreach($results as $dataArr) {
                        ?>
                         <form action="" method="POST" class="form-horizontal form-label-left"  enctype="multipart/form-data" >

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="testCategoryId">Test Category: <span class="requireF">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                 <select name="testCategoryId" class="form-control" autofocus id="testCategoryId" required >
                                    <option value=""> Select test category</option>
                                      <?php
                                        $results = $db_handle->getTestCategoryList();
                                     foreach($results as $dataArr1) {
                                      ?>
                                         <option <?php if($dataArr['testCategoryId']==$dataArr1['id']){ echo "selected";}?> value="<?php echo $dataArr1['id']; ?>"><?php echo $dataArr1['testCategoryName']; ?></option>
                                      <?php } ?>
                                  </select>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Test Name: <span class="requireF">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input value="<?php echo $dataArr['testName'];?>" name="testName" autofocus class="form-control" required type="text" placeholder="Insert test name here ...">
                                <input value="<?php echo $id;?>" name="id" hidden >
                                <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="testDescription">Description:
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea  rows="6" id="testDescription" name="testDescription" class="form-control" placeholder="Insert test description here ..."><?php echo $dataArr['testDescription'];?></textarea>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="testPrice">Test Price: <span class="requireF">*</span>
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="testPrice"  value="<?php echo $dataArr['testPrice'];?>"  required name="testPrice" class="form-control has-feedback" type="number" step="any" placeholder="Insert test price here ..."><span class="form-control-feedback right" aria-hidden="true"><?php echo CURRENCY;?></span>
                              </div>
                            </div>

                            <div class="form-group">
                              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="testLocation">Location:
                              </label>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <input id="testLocation" name="testLocation" required class="form-control"  value="<?php echo $dataArr['testLocation'];?>"  type="text" placeholder="Insert location here ...">
                              </div>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="form-group" style='margin-top:30px;'>
                              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                              <button class="btn btn-success"  name="saveInfo" type="submit" ><i class="fa fa-save "></i> Update Information</button>
                              <a href="test_list.php" class="btn btn-primary "><i class="fa fa-undo "></i> Go Back</a>
                              </div>
                            </div>
                                
                          </form>
                          <?php }} ?>
                          <?php } ?>
                      </center>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>