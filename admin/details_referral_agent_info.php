<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Details of Referral agent</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                   <table class="table table-bordered table-hover table-striped" >
                       <?php 
                        $id = base64_decode($_REQUEST['id']);
                        $results = $db_handle->getReferralAgentDetailsById($id);
                        if(count($results)>0){
                            foreach($results as $tReferralAgentDetails) {
                        ?>
                        <tr><td width="22%">Referral Agent Name:</td><td><b><?php echo $doctorName=htmlentities($tReferralAgentDetails["name"]); ?></b></td></tr>
                        <tr><td>Referral Agent Phone:</td><td><b><?php echo htmlentities($tReferralAgentDetails["phone"]); ?></b></td></tr>
                        <tr><td>Referral Agent Email:</td><td><b><?php echo htmlentities($tReferralAgentDetails["email"]); ?></b></td></tr>
                        <tr><td>Referral Agent Address:</td><td><b><?php echo htmlentities($tReferralAgentDetails["address"]); ?></b></td></tr>
                        <tr><td>Agent Contact Person:</td><td><b><?php echo htmlentities($tReferralAgentDetails["contactPersonName"]); ?></b></td></tr>
                        <tr><td>Agent Contact Phone:</td><td><b><?php echo htmlentities($tReferralAgentDetails["contactPhone"]); ?></b></td></tr>
                        <?php $commissionPercent = $tReferralAgentDetails["commissionPercent"]; }} ?>
                      </table>
                      <hr>
                      <h4 style="font-weight:bold;">Referred Tests List</h4>
                      <table id="datatable"  class="table table-bordered table-hover table-striped"  >
                      <?php 
                        $i=0;
                        $tcamount=0;
                        $tamount=0;
                        $id = base64_decode($_REQUEST['id']);
                        $results = $db_handle->getReferredTestListById1($id);
                        if(count($results)>0){
                          ?>
                        <thead>
                          <th width="7%">Serial</th>
                          <th width="18%">Patient ID</th>
                          <th width="12%">Date</th>
                          <th>Test Name</th>
                          <th width="15%">Test Amount</th>
                          <th width="15%">Com. Percent</th>
                          <th width="15%">Com. Amount</th>
                        </thead>
                        <tbody>
                          <?php
                            foreach($results as $ReferredTestList) {
                          ?>
                      <tr>
                        <td><?php echo ++$i; ?></td>
                        <td><?php echo htmlentities($ReferredTestList["patientId"]); ?></td>
                        <td><?php echo htmlentities(date('d-m-Y', strtotime($ReferredTestList['testDate']))); ?></td>
                        <td><?php echo htmlentities($ReferredTestList["testName"]); ?></td>
                        <td><?php echo htmlentities($ReferredTestList["testPrice"])." Tk"; ?></td>
                        <td><?php echo $commissionPercent." %"; ?></td>
                        <td><?php echo $cam=($ReferredTestList['testPrice'])*($commissionPercent/100)." Tk"; ?></td>
                      </tr>
                       
                       <?php $tcamount +=$cam; $tamount +=$ReferredTestList["testPrice"];  } ?>
                         </tbody>
                        <tfoot>
                        <tr style="font-weight:bold;">
                        <td colspan="3"></td>
                         <td><?php echo "Total:"; ?></td>
                        <td><?php echo $tamount." Tk"; ?></td>
                        <td><?php echo "-"; ?></td>
                        <td><?php echo $tcamount." Tk"; ?></td>
                      </tr>
                      <?php }else{ echo "<h5 style='padding-top:25px;'>No Data Found.</h5>";} ?>
                      </tfoot>
                      </table>

                      <center><a href="referral_agent_list.php" class="btn btn-primary btn-round"> <i class="fa fa-users"></i> All Referral Agent List</a></center>

          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>