<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>

    <?php include("css.php");?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Report From (<b><?php echo date('d-m-Y', strtotime($_POST['fromDate']));?></b>) To (<?php echo date('d-m-Y', strtotime($_POST['toDate']));?>)</h2>
                    
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    
                      <h4><b>Appointment List</b></h4>
                      <table id="datatable" class="table table-striped table-bordered" style="font-size:12px;" >
                      <thead>
                          <th  width="4%">SN</th>
                          <th  width="15%">Patient ID</th>
                          <th  width="15%">Appointment Booking Date</th>
                          <th  width="10%">Appointment Date</th>
                          <th  width="10%">Appointment Time</th>
                          <th width="8%">Amount</th>
                        </thead>
                        <tbody>
                       <?php 
                       $fromDate = $_POST['fromDate'];
                       $toDate = $_POST['toDate'];
                       $tamount=0;
                       $results1 = $db_handle->getDateWiseReportAllAppointmentHistory($fromDate,$toDate);
                       $i=0;
                       if(count($results1)>0){
                          foreach($results1 as $dataArr) {
                        ?> 
                        <tr>
                          <td><?php echo ++$i; ?></td>
                          <td><?php echo $dataArr['patientId']; ?></td>
                          <td><?php echo date('d-m-Y', strtotime($dataArr['createDate'])); ?></td>
                          <td><?php echo date('d-m-Y', strtotime($dataArr['apDate'])); ?></td>
                          <td><?php echo $dataArr['apTime']; ?></td>
                          <td><?php echo $dataArr['amount']." Tk"; ?></td>
                        </tr>
                        <?php 
                          $tamount +=$dataArr['amount'];
                         }
                        } ?>
                        </tbody>
                        </table>
                      <center>  <h5 style="color:red;font-weight:bold;">Total Amount: <?php echo $tamount;?> Taka</h5></center>

                        <hr>
                     <h4><b>Test List</b></h4>

                    <table   id="datatable1" class="table table-striped table-bordered" style="font-size:12px;" >
                      <thead>
                          <th  width="5%">SN</th>
                          <th  width="15%">Patient ID</th>
                          <th  width="15%">Test Booking Date</th>
                          <th  width="15%">Test Name</th>
                          <th  width="10%">Test Date</th>
                          <th  width="10%">Test Time</th>
                          <th width="8%">Amount</th>
                        </thead>
                        <tbody>
                       <?php 
                       $tamount=0;
                       $results1 = $db_handle->getDateWiseReportAllTestHistory($fromDate,$toDate);
                       $i=0;
                       if(count($results1)>0){
                          foreach($results1 as $dataArr) {
                        ?> 
                        <tr>
                          <td><?php echo ++$i; ?></td>
                          <td><?php echo $dataArr['patientId']; ?></td>
                          <td><?php echo date('d-m-Y', strtotime($dataArr['createDate'])); ?></td>
                          <td><?php echo $dataArr['testName']; ?></td>
                          <td><?php echo date('d-m-Y', strtotime($dataArr['testDate'])); ?></td>
                          <td><?php echo $dataArr['testTime']; ?></td>
                          <td><?php echo $dataArr['testPrice']." Tk"; ?></td>
                         
                        </tr>
                        <?php 
                          $tamount +=$dataArr['testPrice'];
                         }
                        } ?>
                        </tbody>
                        </table>
                      <center><h5 style="color:red;font-weight:bold;">Total Amount: <?php echo $tamount;?> Taka</h5></center>

                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>