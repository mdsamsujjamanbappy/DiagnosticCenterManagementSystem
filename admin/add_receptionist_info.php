<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add New Receptionist</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="x_panel">
                            <div class="x_title">
                              <span style="color:black !important;font-weight: bold;font-size: 16px;">Add New Receptionist</span>
                              <ul class="nav navbar-right panel_toolbox">
                                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                </li>
                                <li><a class="close-link"><i class="fa fa-close"></i></a>
                                </li>
                              </ul>
                              <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                              
                             <center>
                               <?php 
                               if (isset($_POST['saveInfo'])) {
                                  if(isset($_POST['_MSBtoken'])){

                                $userFullName = ($_POST['userFullName']);
                                $userPhone = ($_POST['userPhone']);
                                $userEmail = ($_POST['userEmail']);
                                $userJoiningDate = ($_POST['userJoiningDate']);
                                $userAddress = ($_POST['userAddress']);
                                $userName = ($_POST['userName']);
                                $userPassword = ($_POST['userPassword']);
                                $userImage = "udefault.png";

                                if(!empty($_FILES['userImage']['name'])){
                                    if(($_FILES['userImage']['size']) <= (3000*1024))
                                    {
                                          $ddata = date("Ymdhsi");
                                           $picture_tmp = $_FILES['userImage']['tmp_name'];
                                            $picture_name = $_FILES['userImage']['name'];
                                            $picture_type = $_FILES['userImage']['type'];
                                           
                                            $arr1 = explode(".", $picture_tmp);
                                            $extension1 = strtolower(array_pop($arr1));  
                                            
                                            $arr = explode(".", $picture_name);
                                            $extension = strtolower(array_pop($arr));
              
                                            $userImage="user".$ddata."".$phone .".".$extension;
                                            $newfilename1="user".$ddata."".$phone .".".$extension1;
                                            $path = '../user_images/'.$userImage;

                                            move_uploaded_file($picture_tmp, $path);
                                    }
                                  }

                                  $userTypeId = 3;
                                $r = $db_handle->saveUserInformation($userFullName,$userTypeId,$userPhone,$userEmail,$userJoiningDate,$userAddress,$userName,$userPassword,$userImage);

                                if($r){
                                      echo "<script> document.location.href='receptionist_confirmation.php?ssuccess=success';</script>";
                                   
                                }else{
                                    echo "<h2 style='color:red;'>Insertion Failed!!</h2>";

                                    echo "<br />";
                                    echo "<br />";

                                    echo "<a href='add_receptionist_info.php' style='margin-bottom:8px; margin-top:-5px;'' class='btn btn-sm btn-primary'>Try Again</a> &nbsp;&nbsp;";
                                  }
                                }else{
                                    echo "Invalid Token";
                                }
                               }else{ ?>
                                   <form action="" method="POST" class="form-horizontal form-label-left"  enctype="multipart/form-data" >

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Receptionist Name: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input name="userFullName" autofocus class="form-control" required type="text" placeholder="Insert receptionist name here ...">
                                          <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Receptionist Phone: <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input required name="userPhone" class="form-control" type="text" placeholder="Insert receptionist phone here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userEmail">Receptionist Email:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="userEmail" name="userEmail" class="form-control" type="text" placeholder="Insert receptionist email address here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userJoiningDate">Receptionist Joining Date:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="userJoiningDate" name="userJoiningDate" required class="form-control" type="text" placeholder="Insert receptionist joining date  here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userAddress">Receptionist Address:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <textarea id="userAddress" name="userAddress" class="form-control" placeholder="Insert receptionist address here ..."></textarea>
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userName">Username(For Login): <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="userName" name="userName" autofocus class="form-control" required type="text" placeholder="Insert receptionist username here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userPassword">Password(For Login): <span class="requireF">*</span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="userPassword" name="userPassword" autofocus class="form-control" required type="password" placeholder="Insert receptionist password here ...">
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="userImage">Receptionist Image:
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input id="userImage" name="userImage"  class="form-control btn btn-info" type="file" >
                                        </div>
                                      </div>

                                      <div class="ln_solid"></div>

                                      <div class="form-group" style='margin-top:30px;'>
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button class="btn btn-success btn-round "  name="saveInfo" type="submit" ><i class="fa fa-save "></i> Save Information</button>
                                        <a href="receptionist_list.php" class="btn btn-primary btn-round "><i class="fa fa-undo "></i> Go Back</a>
                                        </div>
                                      </div>
                                          
                                    </form>
                                    <?php } ?>
                                </center>

                            </div>
                          </div>
                        </div>
                        </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

  </body>
</html>