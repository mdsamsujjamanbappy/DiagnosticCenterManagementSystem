<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Add New Referral Agent</title>

    <?php include("css.php");?>

     </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Admin Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->userImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->fullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->adminMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->userImage();?>" alt=""><?php $my_tools->fullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                      <h4 style='font-weight:bold;'><i class="fa fa-bullseye"></i> Add New Referral Agent Information</h4>
                      <hr>
                     <center>
                       <?php 
                       if (isset($_POST['saveInfo'])) {
                          if(isset($_POST['_MSBtoken'])){

                        $referralName = ($_POST['referralName']);
                        $referralCPercent = ($_POST['referralCPercent']);
                        $referralPhone = ($_POST['referralPhone']);
                        $referralEmail = ($_POST['referralEmail']);
                        $referralAddress = ($_POST['referralAddress']);
                        $referralCperson = ($_POST['referralCperson']);
                        $referralCPhone = ($_POST['referralCPhone']);
                        $addedBy = $_SESSION['adHMSId'];

                        $r = $db_handle->saveReferralInformation($referralName,$referralCPercent,$referralPhone,$referralEmail,$referralAddress,$referralCperson,$referralCPhone,$addedBy);

                        if($r){
                              echo "<script>document.location.href='referral_agent_list.php?ssuccess=success';</script>";
                           
                        }else{
                            echo "<h2 style='color:red;'>Insertion Failed!!</h2>";
                          }
                        }else{
                            echo "Invalid Token";
                        }
                       }else{ ?>
                           <form action="" method="POST" class="form-horizontal form-label-left"  enctype="multipart/form-data" >

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Referral Agent Name: <span class="requireF">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input name="referralName" autofocus class="form-control" required type="text" placeholder="Insert referral agent name here ...">
                                  <input name="_MSBtoken" hidden value="<?php echo md5(rand(14446,50200));?>">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralCPercent">Commission Percentage:<span class="requireF">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input  id="referralCPercent" required name="referralCPercent" type="number" step="any" class="form-control" placeholder="Insert referral agent commission percentage here ...">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralPhone">Referral Agent Phone:<span class="requireF">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input  id="referralPhone" required name="referralPhone" class="form-control" placeholder="Insert referral phone here ...">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralEmail">Referral Agent Email: 
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input id="referralEmail" name="referralEmail" class="form-control" type="email" placeholder="Insert referral agent email here ...">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralAddress">Referral Agent Address:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <textarea  id="referralAddress" name="referralAddress" class="form-control" placeholder="Insert referral agent address here ..."></textarea>
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralCperson">Agent Contact Person:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input  id="referralCperson" required name="referralCperson" class="form-control" placeholder="Insert referral contact person name here ...">
                                </div>
                              </div>

                              <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="referralCPhone">Contact Person Phone:
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                  <input  id="referralCPhone" required name="referralCPhone" class="form-control" placeholder="Insert referral contact person phone name here ...">
                                </div>
                              </div>


                              <div class="ln_solid"></div>

                              <div class="form-group" style='margin-top:30px;'>
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-success btn-round "  name="saveInfo" type="submit" ><i class="fa fa-save "></i> Save Information</button>
                                <a href="referral_agent_list.php" class="btn btn-primary btn-round "><i class="fa fa-undo "></i> Go Back</a>
                                </div>
                              </div>
                                  
                            </form>
                            <?php } ?>
                        </center>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>

  </body>
</html>