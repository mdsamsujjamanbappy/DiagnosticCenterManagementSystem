DROP TABLE tbdepartment;

CREATE TABLE `tbdepartment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `departmentName` varchar(200) NOT NULL,
  `departmentDescription` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `departmentName` (`departmentName`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO tbdepartment VALUES("8","Dental","");



DROP TABLE tbdoctors;

CREATE TABLE `tbdoctors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `designation` varchar(200) NOT NULL,
  `docDepartmentId` int(1) NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `othersInfo` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `hospitalDetails` text NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `docAppFee` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

INSERT INTO tbdoctors VALUES("1","Prof. Dr. Shahadat Hossain Sheikh","ANNN","6","Uttara","doctor2018013005441801824168996.jpg","MBBS","None","01824168996","doctor","f9f16d97c90d8c6f2cab37bb6d1f1992","402","123456789","600");
INSERT INTO tbdoctors VALUES("6","Mahabubur Rahman","Asst. Professor","6","Uttara","doctor2018012812561301824168966.png","MBBS","Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum","01824168966","Mahabub","09a6f3713ada6ca91362160f287da21f","206","12345678","500");



DROP TABLE tbdoctorshift;

CREATE TABLE `tbdoctorshift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `doctorId` int(11) NOT NULL,
  `day` varchar(25) NOT NULL,
  `time` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO tbdoctorshift VALUES("1","6","Saturday","06:00 PM - 10:00 PM");
INSERT INTO tbdoctorshift VALUES("3","6","Friday","10:00 AM - 12:00 PM");
INSERT INTO tbdoctorshift VALUES("4","6","Monday","10:50 AM - 02:30 PM");
INSERT INTO tbdoctorshift VALUES("5","1","Saturday","10:50AM - 02:30PM");



DROP TABLE tbmedicalhistory;

CREATE TABLE `tbmedicalhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientId` varchar(20) NOT NULL,
  `hblood` int(1) NOT NULL,
  `hcardiac` int(1) NOT NULL,
  `hrheumatic` int(1) NOT NULL,
  `hdiabetes` int(1) NOT NULL,
  `hhapatitis` int(1) NOT NULL,
  `hpregnancy` int(1) NOT NULL,
  `hkidney` int(1) NOT NULL,
  `hhypersensitivity` int(1) NOT NULL,
  `hbloodDisorder` int(1) NOT NULL,
  `hcancer` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patientId` (`patientId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO tbmedicalhistory VALUES("1","201801281056522","1","0","0","0","0","0","0","1","0","0");
INSERT INTO tbmedicalhistory VALUES("2","201801281100068","1","0","0","0","0","0","0","1","0","0");
INSERT INTO tbmedicalhistory VALUES("3","201801281146081","1","0","1","1","1","1","1","1","1","1");
INSERT INTO tbmedicalhistory VALUES("4","201801291001522","1","1","1","1","1","1","1","1","1","1");
INSERT INTO tbmedicalhistory VALUES("5","201801291005421","1","1","1","1","1","1","1","1","1","1");
INSERT INTO tbmedicalhistory VALUES("6","201801300335485","1","0","0","0","0","0","0","0","0","0");
INSERT INTO tbmedicalhistory VALUES("7","201802060144151","1","0","0","1","0","0","0","0","1","0");
INSERT INTO tbmedicalhistory VALUES("8","201802060145505","1","0","0","1","0","1","0","0","1","0");



DROP TABLE tbpatient;

CREATE TABLE `tbpatient` (
  `tmpId` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `address` text NOT NULL,
  `addDate` date NOT NULL,
  `occupation` varchar(50) NOT NULL,
  `father` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `blood` varchar(5) NOT NULL,
  `age` varchar(4) NOT NULL,
  `dob` varchar(20) NOT NULL,
  PRIMARY KEY (`tmpId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO tbpatient VALUES("1","201801281056522","Mahabubur Rahman","01824168966","bappy428@gmail.com","","Uttara","2018-01-28","সফটওয়্যার ইঞ্জিনিয়ার","à¦¦à¦¿à¦®à§ à¦¨à¦¾","Male","O+","23","1994-04-05");
INSERT INTO tbpatient VALUES("2","201801281100068","Samsujjaman Bappy","01824168966","bappy428@gmail.com","","Utrrata","2018-01-28","SOftware","ABC","Male","o+","23","2018-01-28");
INSERT INTO tbpatient VALUES("3","201801281146081","Mr. Bandanewaz","01824168966","samsujjamanbappy@gmail.com","","Uttara","2018-01-28","software","","Male","O+","23","2018-01-28");
INSERT INTO tbpatient VALUES("4","201801291001522","Tamim Rahman","01582311221","tamim@feits.co","","Uttara","2018-01-29","Developer","ABC","Male","O+","24","2018-01-22");
INSERT INTO tbpatient VALUES("5","201801291005421","Yashna Khanom","01824168996","yashna@feits.co","","Dhaka 1230","2018-01-29","SEO","","Female","O+","25","2018-01-01");
INSERT INTO tbpatient VALUES("6","201801300335485","Md. Samsujjaman","01515268864","","","কালীগঞ্জ","2018-01-30","সফটওয়্যার ইঞ্জিনিয়ার","কাম্রুজ্জামান বাচ্চু","Male","O+","23","");
INSERT INTO tbpatient VALUES("7","201802060144151","Farjana Tanjim","01554454125","farjana@feits.co","","Uttara 10","2018-02-06","Student","Mr. ABC","Female","O+","22","1995-10-25");
INSERT INTO tbpatient VALUES("8","201802060145505","Muna Rahman","01824168966","Muna@feits.co","","Uttara","2018-02-06","Student","Mr. XYZ","Female","0+","23","01-01-2018");



DROP TABLE tbpatientappointment;

CREATE TABLE `tbpatientappointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `apDate` date NOT NULL,
  `apTime` varchar(20) NOT NULL,
  `doctorId` int(11) NOT NULL,
  `patientId` varchar(25) NOT NULL,
  `amount` varchar(25) NOT NULL DEFAULT '0',
  `receptionistId` int(11) NOT NULL,
  `createDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

INSERT INTO tbpatientappointment VALUES("1","2018-01-28","02:30 PM","6","201801281056522","500","0","2018-01-29");
INSERT INTO tbpatientappointment VALUES("2","2018-01-28","02:35 PM","6","201801281100068","500","0","2018-01-28");
INSERT INTO tbpatientappointment VALUES("3","2018-02-05","02:35 PM","1","201801281146081","600","11","2018-02-15");
INSERT INTO tbpatientappointment VALUES("4","2018-01-30","02:35 PM","1","201801291001522","500","0","2018-02-03");
INSERT INTO tbpatientappointment VALUES("5","2018-01-30","02:45 PM","6","201801291005421","500","0","2018-01-26");
INSERT INTO tbpatientappointment VALUES("6","2018-01-30","02:35 PM","1","201801300335485","500","11","2018-01-30");
INSERT INTO tbpatientappointment VALUES("7","2018-01-28","02:35 PM","1","201802060144151","600","11","2018-02-06");
INSERT INTO tbpatientappointment VALUES("8","2018-01-28","02:35 PM","6","201802060145505","500","11","2018-02-06");



DROP TABLE tbpatientmedicine;

CREATE TABLE `tbpatientmedicine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientId` varchar(22) NOT NULL,
  `medicine` mediumtext NOT NULL,
  `uses` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `instruction` mediumtext NOT NULL,
  `pdate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

INSERT INTO tbpatientmedicine VALUES("10","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("11","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("12","201801281146081","Dentistry","1+0+0","7","Before Meal","2017-06-11");
INSERT INTO tbpatientmedicine VALUES("13","201801281146081","Impression","1+0+0","7","Before Meal","2017-06-11");
INSERT INTO tbpatientmedicine VALUES("14","201801281146081","Mexpro","1+1+0","4","Khaiya khaben","2017-06-11");
INSERT INTO tbpatientmedicine VALUES("16","201801300335485","Seclo","1+1+0","7","Before Meal","2018-02-01");
INSERT INTO tbpatientmedicine VALUES("17","201801300335485","Napa","1+1+0","7","After Meal","2018-02-01");
INSERT INTO tbpatientmedicine VALUES("18","201801281146081","Seclo","1+1+0","7","After Meal","2018-02-01");
INSERT INTO tbpatientmedicine VALUES("19","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("20","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("21","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("22","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("23","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("24","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("25","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("26","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("36","201801281146081","Seclo","1+1+0","5","Before eat","2017-05-27");
INSERT INTO tbpatientmedicine VALUES("37","201801281146081","Napa","1+1+0","7","Before eat","2017-05-27");



DROP TABLE tbpatientpayment;

CREATE TABLE `tbpatientpayment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patientId` varchar(30) NOT NULL,
  `pDate` date NOT NULL,
  `amount` varchar(30) NOT NULL DEFAULT '0',
  `creditAmount` varchar(30) NOT NULL DEFAULT '0',
  `pPDescription` text NOT NULL,
  `userId` varchar(20) NOT NULL,
  `tstatus` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO tbpatientpayment VALUES("1","201801281056522","2018-01-28","500","0","","","0");
INSERT INTO tbpatientpayment VALUES("2","201801281100068","2018-01-28","500","0","","","0");
INSERT INTO tbpatientpayment VALUES("3","201801281146081","2018-01-28","600","0","","","0");
INSERT INTO tbpatientpayment VALUES("4","201801281146081","2018-01-28","3500","4500","","","0");
INSERT INTO tbpatientpayment VALUES("5","201801281146081","2018-01-28","200","0","","","0");
INSERT INTO tbpatientpayment VALUES("6","201801281146081","2018-01-28","0","0","","","0");
INSERT INTO tbpatientpayment VALUES("7","201801281100068","2018-01-29","200","250","Payment For Tests","","0");
INSERT INTO tbpatientpayment VALUES("8","201801291001522","2018-01-29","500","0","Consultation Fee","","0");
INSERT INTO tbpatientpayment VALUES("9","201801291005421","2018-01-29","0","500","Consultation Fee","","0");
INSERT INTO tbpatientpayment VALUES("10","201801291005421","2018-01-29","2250","3400","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("11","201801291005421","2018-01-29","3200","3400","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("12","201801281146081","2018-01-30","280","300","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("13","201801300335485","2018-01-30","500","500","Consultation Fee","","0");
INSERT INTO tbpatientpayment VALUES("14","201801281146081","2018-02-01","3200","3400","Payment Against TestsPayment Against TestsPayment Against TestsPayment Against TestsPayment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("15","201801300335485","2018-02-01","250","250","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("16","201801300335485","2018-02-05","200","250","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("17","201801300335485","2018-02-06","200","250","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("18","201802060144151","2018-02-06","600","600","Consultation Fee","","0");
INSERT INTO tbpatientpayment VALUES("19","201802060145505","2018-02-06","0","500","Consultation Fee","","0");
INSERT INTO tbpatientpayment VALUES("20","201802060145505","2018-02-06","500","550","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("21","201802060145505","2018-02-06","500","550","Payment Against Tests","","0");
INSERT INTO tbpatientpayment VALUES("22","201802060145505","2018-02-06","500","550","Payment Against Tests","","1");
INSERT INTO tbpatientpayment VALUES("23","201802060145505","2018-02-06","500","550","Payment Against Tests","","1");
INSERT INTO tbpatientpayment VALUES("24","201802060144151","2018-02-06","200","250","Payment Against Tests","","1");
INSERT INTO tbpatientpayment VALUES("25","201802060144151","2018-02-06","3000","3450","Payment Against Tests","","1");
INSERT INTO tbpatientpayment VALUES("26","201801300335485","2018-02-07","200","250","Payment Against Tests","","1");
INSERT INTO tbpatientpayment VALUES("27","201801300335485","2018-02-07","100","0","Ager Baki","11","0");



DROP TABLE tbpatienttestdetails;

CREATE TABLE `tbpatienttestdetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testId` int(11) NOT NULL,
  `testPrice` varchar(20) NOT NULL,
  `testDate` date NOT NULL,
  `patientId` varchar(20) NOT NULL,
  `testTime` varchar(25) NOT NULL,
  `testRDescription` text NOT NULL,
  `testAttachment` text NOT NULL,
  `labAttendantId` int(11) NOT NULL,
  `receptionistId` int(11) NOT NULL,
  `referredAgentId` varchar(5) NOT NULL,
  `createDate` date NOT NULL,
  `paymentId` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

INSERT INTO tbpatienttestdetails VALUES("1","2","250","2018-01-28","201801281146081","10:00 AM","ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.","1231222121.pdf","6","0","1","2018-01-29","");
INSERT INTO tbpatienttestdetails VALUES("2","3","300","2018-01-28","201801281146081","10:20 AM","It has roots in a piece of classical Latin literatut Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, ","1231222121.pdf","0","0","","0000-00-00","");
INSERT INTO tbpatienttestdetails VALUES("3","1","3150","2018-01-29","201801281146081","10:00 AM","IMPRESSION: 1. No evidence of acute pulmonary pathology. 2. Enlarged cardiac silhouette. 3. Tortuous aorta. 4. Diffusely increased interstitial lung markings consistent with chronic bronchitis. Underlying pulmonary fibrosis is not excluded. 5. Consider follow up with Chest CT if clinically warranted.","12313213215451.jpg","6","0","","2018-01-16","");
INSERT INTO tbpatienttestdetails VALUES("4","2","250","2018-01-31","201801281100068","10:20 AM","","","0","0","","0000-00-00","");
INSERT INTO tbpatienttestdetails VALUES("5","2","250","2018-01-29","201801291005421","10:00 AM","","2018012910054215807.pdf","6","0","","2018-01-28","");
INSERT INTO tbpatienttestdetails VALUES("6","1","3150","2018-01-31","201801291005421","10:20 AM","","2018012910054216353.pdf","6","0","","0000-00-00","");
INSERT INTO tbpatienttestdetails VALUES("7","2","250","2018-01-30","201801291005421","10:00 AM","","2018012910054217474.jpg","6","0","","0000-00-00","");
INSERT INTO tbpatienttestdetails VALUES("8","1","3150","2018-01-30","201801291005421","06:00 PM","","2018012910054218627.docx","6","0","","0000-00-00","");
INSERT INTO tbpatienttestdetails VALUES("9","3","300","2018-01-30","201801281146081","10:20 AM","","","0","11","1","2018-01-30","");
INSERT INTO tbpatienttestdetails VALUES("10","2","250","2018-02-01","201801281146081","10:00 AM","","","0","11","0","2018-02-01","");
INSERT INTO tbpatienttestdetails VALUES("11","1","3150","2018-02-02","201801281146081","10:00 AM","","","0","11","1","2018-02-04","");
INSERT INTO tbpatienttestdetails VALUES("12","2","250","2018-02-01","201801300335485","10:20 AM","","","0","11","1","2018-02-01","");
INSERT INTO tbpatienttestdetails VALUES("13","2","250","2018-04-28","201801300335485","10:20 AM","","","0","11","1","2018-02-05","");
INSERT INTO tbpatienttestdetails VALUES("14","2","250","2018-02-06","201801300335485","10:20 AM","","","0","11","2","2018-02-06","");
INSERT INTO tbpatienttestdetails VALUES("15","3","300","2018-02-06","201802060145505","10:00 AM","","","0","11","2","2018-02-06","20");
INSERT INTO tbpatienttestdetails VALUES("16","2","250","2018-02-06","201802060145505","06:00 PM","","20180206014550516828.pdf","6","11","3","2018-02-06","20");
INSERT INTO tbpatienttestdetails VALUES("17","3","300","2018-02-06","201802060145505","10:00 AM","","","0","11","2","2018-02-06","21");
INSERT INTO tbpatienttestdetails VALUES("18","2","250","2018-02-06","201802060145505","06:00 PM","","","0","11","3","2018-02-06","21");
INSERT INTO tbpatienttestdetails VALUES("19","3","300","2018-02-06","201802060145505","10:00 AM","","","0","11","2","2018-02-06","22");
INSERT INTO tbpatienttestdetails VALUES("20","2","250","2018-02-06","201802060145505","06:00 PM","","","0","11","3","2018-02-06","22");
INSERT INTO tbpatienttestdetails VALUES("21","3","300","2018-02-06","201802060145505","10:00 AM","","","0","11","2","2018-02-06","23");
INSERT INTO tbpatienttestdetails VALUES("22","2","250","2018-02-06","201802060145505","06:00 PM","","","0","11","3","2018-02-06","23");
INSERT INTO tbpatienttestdetails VALUES("23","2","250","2018-02-06","201802060144151","10:20 AM","","","0","11","3","2018-02-06","24");
INSERT INTO tbpatienttestdetails VALUES("24","3","300","2018-02-06","201802060144151","10:00 AM","","","0","11","2","2018-02-06","25");
INSERT INTO tbpatienttestdetails VALUES("25","1","3150","2018-02-07","201802060144151","10:00 AM","","","0","11","3","2018-02-06","25");
INSERT INTO tbpatienttestdetails VALUES("26","2","250","2018-02-07","201801300335485","10:00 AM","","","0","11","0","2018-02-07","26");



DROP TABLE tbreferral_agent;

CREATE TABLE `tbreferral_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `commissionPercent` varchar(20) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contactPersonName` varchar(255) NOT NULL,
  `contactPhone` varchar(25) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `addedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO tbreferral_agent VALUES("1","Popular Diagnostic Center","House #16, Road # 2, Dhanmondi R/A, Dhaka-1205 Bangladesh ","3","+880 9613 787801","info@populardiagnostic.com","Alif Hasan","01565523644","0","2018-02-01 14:27:31");
INSERT INTO tbreferral_agent VALUES("2","Uttara Crescent Diagnostic & Consultation Center","H No. 16, Hannan Complex, Rabindra Sarani, Sector","5","8932430, 8933298, 01917704156","info@uttaracrescenthospitalbd.org","Dr. Nazmin Hossain","01917704156","11","2018-02-01 14:33:23");
INSERT INTO tbreferral_agent VALUES("3","LABAID Diagnostic Center","House # 15, Road # 12, Sector # 6, Dhaka 1230","4","02-58950322","info@labaidgroup.com","Mr. XYZ","01766661900","11","2018-02-01 14:40:47");



DROP TABLE tbtestinfo;

CREATE TABLE `tbtestinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `testName` varchar(255) NOT NULL,
  `testDescription` text NOT NULL,
  `testPrice` varchar(50) NOT NULL,
  `testLocation` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO tbtestinfo VALUES("1","X-ray","none","3150","Room#401");
INSERT INTO tbtestinfo VALUES("2","Blood Test","None","240","Room#409");
INSERT INTO tbtestinfo VALUES("3","Urine Test","None","300","Room#410");
INSERT INTO tbtestinfo VALUES("4","Colonoscopy","Colonoscopy","1220","Room#");
INSERT INTO tbtestinfo VALUES("5","Short Colonoscopy","Short Colonoscopy","980","Room#");
INSERT INTO tbtestinfo VALUES("6","Urine-HCG","","390","Room#");
INSERT INTO tbtestinfo VALUES("7","Sugar (Urine)","","340","Room#");



DROP TABLE tbtesttmp;

CREATE TABLE `tbtesttmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tmpid` varchar(30) NOT NULL,
  `patientId` varchar(30) NOT NULL,
  `testId` int(11) NOT NULL,
  `amount` varchar(30) NOT NULL,
  `pbDate` date NOT NULL,
  `pbTime` varchar(30) NOT NULL,
  `referredAgentId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




DROP TABLE tbtools;

CREATE TABLE `tbtools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `myOption` varchar(255) NOT NULL,
  `myValue` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO tbtools VALUES("1","footertext1","*** Slogan goes here ***");



DROP TABLE tbusers;

CREATE TABLE `tbusers` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `userFullName` varchar(200) NOT NULL,
  `userTypeId` int(1) NOT NULL,
  `userPhone` varchar(50) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userJoiningDate` varchar(50) NOT NULL,
  `userAddress` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userPassword` text NOT NULL,
  `userImage` text NOT NULL,
  PRIMARY KEY (`userid`),
  UNIQUE KEY `userEmail` (`userEmail`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

INSERT INTO tbusers VALUES("4","Hasan ul ferdous","4","01824156689","hasan@feits.co","01-01-2018","Uttara","hasan","fc3f318fba8b3c1502bece62a27712df","user20180124102827.jpg");
INSERT INTO tbusers VALUES("5","Tamim Bokhtiar","4","01824156559","tamim@feits.co","01-01-2018","Uttara 11","tamim","70693e458a02b1810e16112aad100f66","user20180124105446.jpg");
INSERT INTO tbusers VALUES("6","Hasan ul ferdous","5","01824156559","hasan1@feits.co","01-01-2018","House #51, Road #18 Sector #11 Uttara, Dhaka-1230","laba","bbfcf4f1c86d2c6adcb3a4d7632bb16e","user2018013004485801824156559.jpg");
INSERT INTO tbusers VALUES("11","Hasan ul ferdous","3","01824156559","hasan6@feits.co","01-01-2018","House #51, Road #18 Sector #11 Uttara, Dhaka-1230","recep","746035f8741e922eb5fa31ea766d9ece","user2018013004485801824156559.jpg");
INSERT INTO tbusers VALUES("12","Admin","1","01824156689","","01-01-2018","Uttara","admin","21232f297a57a5a743894a0e4a801fc3","user20180124102827.jpg");



