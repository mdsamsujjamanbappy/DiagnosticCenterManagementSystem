-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 11, 2018 at 05:46 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbmsb_hms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbdepartment`
--

CREATE TABLE `tbdepartment` (
  `id` int(11) NOT NULL,
  `departmentName` varchar(200) NOT NULL,
  `departmentDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbdepartment`
--

INSERT INTO `tbdepartment` (`id`, `departmentName`, `departmentDescription`) VALUES
(8, 'Dental', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `tbdoctors`
--

CREATE TABLE `tbdoctors` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `designation` varchar(200) NOT NULL,
  `docDepartmentId` int(1) NOT NULL,
  `address` text NOT NULL,
  `image` text NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `othersInfo` text NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(150) NOT NULL,
  `hospitalDetails` text NOT NULL,
  `registrationNumber` varchar(250) NOT NULL,
  `docAppFee` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbdoctors`
--

INSERT INTO `tbdoctors` (`id`, `name`, `designation`, `docDepartmentId`, `address`, `image`, `qualification`, `othersInfo`, `phone`, `email`, `password`, `hospitalDetails`, `registrationNumber`, `docAppFee`) VALUES
(1, 'Prof. Dr. Shahadat Hossain Sheikh', 'ANNN', 6, 'Uttara', 'doctor2018013005441801824168996.jpg', 'MBBS', 'None', '01824168996', 'doctor', 'f9f16d97c90d8c6f2cab37bb6d1f1992', '402', '123456789', '600'),
(6, 'Mahabubur Rahman', 'Asst. Professor', 6, 'Uttara', 'doctor2018012812561301824168966.png', 'MBBS', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '01824168966', 'Mahabub', '09a6f3713ada6ca91362160f287da21f', '206', '12345678', '500');

-- --------------------------------------------------------

--
-- Table structure for table `tbdoctorshift`
--

CREATE TABLE `tbdoctorshift` (
  `id` int(11) NOT NULL,
  `doctorId` int(11) NOT NULL,
  `day` varchar(25) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbdoctorshift`
--

INSERT INTO `tbdoctorshift` (`id`, `doctorId`, `day`, `time`) VALUES
(1, 6, 'Saturday', '06:00 PM - 10:00 PM'),
(3, 6, 'Friday', '10:00 AM - 12:00 PM'),
(4, 6, 'Monday', '10:50 AM - 02:30 PM'),
(5, 1, 'Saturday', '10:50AM - 02:30PM');

-- --------------------------------------------------------

--
-- Table structure for table `tbmedicalhistory`
--

CREATE TABLE `tbmedicalhistory` (
  `id` int(11) NOT NULL,
  `patientId` varchar(20) NOT NULL,
  `hblood` int(1) NOT NULL,
  `hcardiac` int(1) NOT NULL,
  `hrheumatic` int(1) NOT NULL,
  `hdiabetes` int(1) NOT NULL,
  `hhapatitis` int(1) NOT NULL,
  `hpregnancy` int(1) NOT NULL,
  `hkidney` int(1) NOT NULL,
  `hhypersensitivity` int(1) NOT NULL,
  `hbloodDisorder` int(1) NOT NULL,
  `hcancer` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmedicalhistory`
--

INSERT INTO `tbmedicalhistory` (`id`, `patientId`, `hblood`, `hcardiac`, `hrheumatic`, `hdiabetes`, `hhapatitis`, `hpregnancy`, `hkidney`, `hhypersensitivity`, `hbloodDisorder`, `hcancer`) VALUES
(1, '201801281056522', 1, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(2, '201801281100068', 1, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(3, '201801281146081', 1, 0, 1, 1, 1, 1, 1, 1, 1, 1),
(4, '201801291001522', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(5, '201801291005421', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(6, '201801300335485', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(7, '201802060144151', 1, 0, 0, 1, 0, 0, 0, 0, 1, 0),
(8, '201802060145505', 1, 0, 0, 1, 0, 1, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbmedicine`
--

CREATE TABLE `tbmedicine` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `companyName` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbmedicine`
--

INSERT INTO `tbmedicine` (`id`, `name`, `companyName`, `description`) VALUES
(1, 'Napa', '', ''),
(2, 'Seclo', '', ''),
(3, 'Napa Extra', 'beximcopharma', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbpatient`
--

CREATE TABLE `tbpatient` (
  `tmpId` int(11) NOT NULL,
  `id` varchar(50) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(60) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `address` text NOT NULL,
  `addDate` date NOT NULL,
  `occupation` varchar(50) NOT NULL,
  `father` varchar(50) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `blood` varchar(5) NOT NULL,
  `age` varchar(4) NOT NULL,
  `dob` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpatient`
--

INSERT INTO `tbpatient` (`tmpId`, `id`, `name`, `phone`, `email`, `password`, `address`, `addDate`, `occupation`, `father`, `gender`, `blood`, `age`, `dob`) VALUES
(1, '201801281056522', 'Mahabubur Rahman', '01824168966', 'bappy428@gmail.com', '', 'Uttara', '2018-01-28', 'সফটওয়্যার ইঞ্জিনিয়ার', 'à¦¦à¦¿à¦®à§ à¦¨à¦¾', 'Male', 'O+', '23', '1994-04-05'),
(2, '201801281100068', 'Samsujjaman Bappy', '01824168966', 'bappy428@gmail.com', '', 'Utrrata', '2018-01-28', 'SOftware', 'ABC', 'Male', 'o+', '23', '2018-01-28'),
(3, '201801281146081', 'Mr. Bandanewaz', '01824168966', 'samsujjamanbappy@gmail.com', '', 'Uttara', '2018-01-28', 'software', '', 'Male', 'O+', '23', '2018-01-28'),
(4, '201801291001522', 'Tamim Rahman', '01582311221', 'tamim@feits.co', '', 'Uttara', '2018-01-29', 'Developer', 'ABC', 'Male', 'O+', '24', '2018-01-22'),
(5, '201801291005421', 'Yashna Khanom', '01824168996', 'yashna@feits.co', '', 'Dhaka 1230', '2018-01-29', 'SEO', '', 'Female', 'O+', '25', '2018-01-01'),
(6, '201801300335485', 'Md. Samsujjaman', '01515268864', '', '', 'কালীগঞ্জ', '2018-01-30', 'সফটওয়্যার ইঞ্জিনিয়ার', 'কাম্রুজ্জামান বাচ্চু', 'Male', 'O+', '23', ''),
(7, '201802060144151', 'Farjana Tanjim', '01554454125', 'farjana@feits.co', '', 'Uttara 10', '2018-02-06', 'Student', 'Mr. ABC', 'Female', 'O+', '22', '1995-10-25'),
(8, '201802060144151', 'Muna Rahman', '01824168966', 'Muna@feits.co', '', 'Uttara', '2018-02-06', 'Student', 'Mr. XYZ', 'Female', '0+', '23', '01-01-2018');

-- --------------------------------------------------------

--
-- Table structure for table `tbpatientappointment`
--

CREATE TABLE `tbpatientappointment` (
  `id` int(11) NOT NULL,
  `apDate` date NOT NULL,
  `apTime` varchar(20) NOT NULL,
  `doctorId` int(11) NOT NULL,
  `patientId` varchar(25) NOT NULL,
  `amount` varchar(25) NOT NULL DEFAULT '0',
  `receptionistId` int(11) NOT NULL,
  `createDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpatientappointment`
--

INSERT INTO `tbpatientappointment` (`id`, `apDate`, `apTime`, `doctorId`, `patientId`, `amount`, `receptionistId`, `createDate`) VALUES
(1, '2018-01-28', '02:30 PM', 6, '201801281056522', '500', 0, '2018-01-29'),
(2, '2018-01-28', '02:35 PM', 6, '201801281100068', '500', 0, '2018-01-28'),
(3, '2018-02-05', '02:35 PM', 1, '201801281146081', '600', 11, '2018-02-15'),
(4, '2018-01-30', '02:35 PM', 1, '201801291001522', '500', 0, '2018-02-03'),
(5, '2018-01-30', '02:45 PM', 6, '201801291005421', '500', 0, '2018-01-26'),
(6, '2018-01-30', '02:35 PM', 1, '201801300335485', '500', 11, '2018-01-30'),
(7, '2018-01-28', '02:35 PM', 1, '201802060144151', '600', 11, '2018-02-06'),
(8, '2018-01-28', '02:35 PM', 6, '201802060145505', '500', 11, '2018-02-06');

-- --------------------------------------------------------

--
-- Table structure for table `tbpatientmedicine`
--

CREATE TABLE `tbpatientmedicine` (
  `id` int(11) NOT NULL,
  `patientId` varchar(22) NOT NULL,
  `medicine` mediumtext NOT NULL,
  `uses` varchar(20) NOT NULL,
  `days` varchar(20) NOT NULL,
  `instruction` mediumtext NOT NULL,
  `pdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpatientmedicine`
--

INSERT INTO `tbpatientmedicine` (`id`, `patientId`, `medicine`, `uses`, `days`, `instruction`, `pdate`) VALUES
(10, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(11, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(12, '201801281146081', 'Dentistry', '1+0+0', '7', 'Before Meal', '2017-06-11'),
(13, '201801281146081', 'Impression', '1+0+0', '7', 'Before Meal', '2017-06-11'),
(14, '201801281146081', 'Mexpro', '1+1+0', '4', 'Khaiya khaben', '2017-06-11'),
(16, '201801300335485', 'Seclo', '1+1+0', '7', 'Before Meal', '2018-02-01'),
(17, '201801300335485', 'Napa', '1+1+0', '7', 'After Meal', '2018-02-01'),
(18, '201801281146081', 'Seclo', '1+1+0', '7', 'After Meal', '2018-02-01'),
(19, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(20, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(21, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(22, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(23, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(24, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(25, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(26, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(36, '201801281146081', 'Seclo', '1+1+0', '5', 'Before eat', '2017-05-27'),
(37, '201801281146081', 'Napa', '1+1+0', '7', 'Before eat', '2017-05-27'),
(38, '201801281056522', 'Napa Extra', '1+1+0', '', '', '2018-03-11'),
(39, '201801281056522', 'Seclo', '1+1+1', '7', 'BB', '2018-03-11');

-- --------------------------------------------------------

--
-- Table structure for table `tbpatientpayment`
--

CREATE TABLE `tbpatientpayment` (
  `id` int(11) NOT NULL,
  `patientId` varchar(30) NOT NULL,
  `pDate` date NOT NULL,
  `amount` varchar(30) NOT NULL DEFAULT '0',
  `creditAmount` varchar(30) NOT NULL DEFAULT '0',
  `pPDescription` text NOT NULL,
  `userId` varchar(20) NOT NULL,
  `tstatus` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpatientpayment`
--

INSERT INTO `tbpatientpayment` (`id`, `patientId`, `pDate`, `amount`, `creditAmount`, `pPDescription`, `userId`, `tstatus`) VALUES
(1, '201801281056522', '2018-01-28', '500', '0', '', '', 0),
(2, '201801281100068', '2018-01-28', '500', '0', '', '', 0),
(3, '201801281146081', '2018-01-28', '600', '0', '', '', 0),
(4, '201801281146081', '2018-01-28', '3500', '4500', '', '', 0),
(5, '201801281146081', '2018-01-28', '200', '0', '', '', 0),
(6, '201801281146081', '2018-01-28', '0', '0', '', '', 0),
(7, '201801281100068', '2018-01-29', '200', '250', 'Payment For Tests', '', 0),
(8, '201801291001522', '2018-01-29', '500', '0', 'Consultation Fee', '', 0),
(9, '201801291005421', '2018-01-29', '0', '500', 'Consultation Fee', '', 0),
(10, '201801291005421', '2018-01-29', '2250', '3400', 'Payment Against Tests', '', 0),
(11, '201801291005421', '2018-01-29', '3200', '3400', 'Payment Against Tests', '', 0),
(12, '201801281146081', '2018-01-30', '280', '300', 'Payment Against Tests', '', 0),
(13, '201801300335485', '2018-01-30', '500', '500', 'Consultation Fee', '', 0),
(14, '201801281146081', '2018-02-01', '3200', '3400', 'Payment Against TestsPayment Against TestsPayment Against TestsPayment Against TestsPayment Against Tests', '', 0),
(15, '201801300335485', '2018-02-01', '250', '250', 'Payment Against Tests', '', 0),
(16, '201801300335485', '2018-02-05', '200', '250', 'Payment Against Tests', '', 0),
(17, '201801300335485', '2018-02-06', '200', '250', 'Payment Against Tests', '', 0),
(18, '201802060144151', '2018-02-06', '600', '600', 'Consultation Fee', '', 0),
(19, '201802060145505', '2018-02-06', '0', '500', 'Consultation Fee', '', 0),
(20, '201802060145505', '2018-02-06', '500', '550', 'Payment Against Tests', '', 0),
(21, '201802060145505', '2018-02-06', '500', '550', 'Payment Against Tests', '', 0),
(22, '201802060145505', '2018-02-06', '500', '550', 'Payment Against Tests', '', 1),
(23, '201802060145505', '2018-02-06', '500', '550', 'Payment Against Tests', '', 1),
(24, '201802060144151', '2018-02-06', '200', '250', 'Payment Against Tests', '', 1),
(25, '201802060144151', '2018-02-06', '3000', '3450', 'Payment Against Tests', '', 1),
(26, '201801300335485', '2018-02-07', '200', '250', 'Payment Against Tests', '', 1),
(27, '201801300335485', '2018-02-07', '100', '0', 'Ager Baki', '11', 0),
(28, '201801281056522', '2018-03-08', '240', '240', 'Payment Against Tests', '', 1),
(29, '201801281056522', '2018-03-08', '0', '3540', 'Payment Against Tests', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbpatienttestdetails`
--

CREATE TABLE `tbpatienttestdetails` (
  `id` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  `testPrice` varchar(20) NOT NULL,
  `testDate` date NOT NULL,
  `patientId` varchar(20) NOT NULL,
  `testTime` varchar(25) NOT NULL,
  `testRDescription` text NOT NULL,
  `testAttachment` text NOT NULL,
  `labAttendantId` int(11) NOT NULL,
  `receptionistId` int(11) NOT NULL,
  `referredAgentId` varchar(5) NOT NULL,
  `createDate` date NOT NULL,
  `paymentId` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbpatienttestdetails`
--

INSERT INTO `tbpatienttestdetails` (`id`, `testId`, `testPrice`, `testDate`, `patientId`, `testTime`, `testRDescription`, `testAttachment`, `labAttendantId`, `receptionistId`, `referredAgentId`, `createDate`, `paymentId`) VALUES
(1, 2, '250', '2018-01-28', '201801281146081', '10:00 AM', 'ontrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', '1231222121.pdf', 6, 0, '1', '2018-01-29', ''),
(2, 3, '300', '2018-01-28', '201801281146081', '10:20 AM', 'It has roots in a piece of classical Latin literatut Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, ', '1231222121.pdf', 0, 0, '', '0000-00-00', ''),
(3, 1, '3150', '2018-01-29', '201801281146081', '10:00 AM', 'IMPRESSION: 1. No evidence of acute pulmonary pathology. 2. Enlarged cardiac silhouette. 3. Tortuous aorta. 4. Diffusely increased interstitial lung markings consistent with chronic bronchitis. Underlying pulmonary fibrosis is not excluded. 5. Consider follow up with Chest CT if clinically warranted.', '12313213215451.jpg', 6, 0, '', '2018-01-16', ''),
(4, 2, '250', '2018-01-31', '201801281100068', '10:20 AM', '', '', 0, 0, '', '0000-00-00', ''),
(5, 2, '250', '2018-01-29', '201801291005421', '10:00 AM', '', '2018012910054215807.pdf', 6, 0, '', '2018-01-28', ''),
(6, 1, '3150', '2018-01-31', '201801291005421', '10:20 AM', '', '2018012910054216353.pdf', 6, 0, '', '0000-00-00', ''),
(7, 2, '250', '2018-01-30', '201801291005421', '10:00 AM', '', '2018012910054217474.jpg', 6, 0, '', '0000-00-00', ''),
(8, 1, '3150', '2018-01-30', '201801291005421', '06:00 PM', '', '2018012910054218627.docx', 6, 0, '', '0000-00-00', ''),
(9, 3, '300', '2018-01-30', '201801281146081', '10:20 AM', '', '', 0, 11, '1', '2018-01-30', ''),
(10, 2, '250', '2018-02-01', '201801281146081', '10:00 AM', '', '', 0, 11, '0', '2018-02-01', ''),
(11, 1, '3150', '2018-02-02', '201801281146081', '10:00 AM', '', '', 0, 11, '1', '2018-02-04', ''),
(12, 2, '250', '2018-02-01', '201801300335485', '10:20 AM', '', '', 0, 11, '1', '2018-02-01', ''),
(13, 2, '250', '2018-04-28', '201801300335485', '10:20 AM', '', '', 0, 11, '1', '2018-02-05', ''),
(14, 2, '250', '2018-02-06', '201801300335485', '10:20 AM', '', '', 0, 11, '2', '2018-02-06', ''),
(15, 3, '300', '2018-02-06', '201802060145505', '10:00 AM', '', '', 0, 11, '2', '2018-02-06', '20'),
(16, 2, '250', '2018-02-06', '201802060145505', '06:00 PM', '', '20180206014550516828.pdf', 6, 11, '3', '2018-02-06', '20'),
(17, 3, '300', '2018-02-06', '201802060145505', '10:00 AM', '', '', 0, 11, '2', '2018-02-06', '21'),
(18, 2, '250', '2018-02-06', '201802060145505', '06:00 PM', '', '', 0, 11, '3', '2018-02-06', '21'),
(19, 3, '300', '2018-02-06', '201802060145505', '10:00 AM', '', '', 0, 11, '2', '2018-02-06', '22'),
(20, 2, '250', '2018-02-06', '201802060145505', '06:00 PM', '', '', 0, 11, '3', '2018-02-06', '22'),
(21, 3, '300', '2018-02-06', '201802060145505', '10:00 AM', '', '', 0, 11, '2', '2018-02-06', '23'),
(22, 2, '250', '2018-02-06', '201802060145505', '06:00 PM', '', '', 0, 11, '3', '2018-02-06', '23'),
(23, 2, '250', '2018-02-06', '201802060144151', '10:20 AM', '', '2018020601441512366.docx', 6, 11, '3', '2018-02-06', '24'),
(24, 3, '300', '2018-02-06', '201802060144151', '10:00 AM', '', '', 0, 11, '2', '2018-02-06', '25'),
(25, 1, '3150', '2018-02-07', '201802060144151', '10:00 AM', '', '', 0, 11, '3', '2018-02-06', '25'),
(26, 2, '250', '2018-02-07', '201801300335485', '10:00 AM', '', '', 0, 11, '0', '2018-02-07', '26'),
(27, 9, '240', '2018-03-08', '201801281056522', '10:00 AM', '', '', 0, 11, '0', '2018-03-08', '28'),
(28, 1, '3150', '2018-03-08', '201801281056522', '10:20 AM', '', '', 0, 11, '1', '2018-03-08', '29'),
(29, 6, '390', '2018-03-08', '201801281056522', '10:00 AM', '', '', 0, 11, '0', '2018-03-08', '29');

-- --------------------------------------------------------

--
-- Table structure for table `tbreferral_agent`
--

CREATE TABLE `tbreferral_agent` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `commissionPercent` varchar(20) NOT NULL DEFAULT '0',
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contactPersonName` varchar(255) NOT NULL,
  `contactPhone` varchar(25) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `addedDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbreferral_agent`
--

INSERT INTO `tbreferral_agent` (`id`, `name`, `address`, `commissionPercent`, `phone`, `email`, `contactPersonName`, `contactPhone`, `addedBy`, `addedDate`) VALUES
(1, 'Popular Diagnostic Center', 'House #16, Road # 2, Dhanmondi R/A, Dhaka-1205 Bangladesh ', '3', '+880 9613 787801', 'info@populardiagnostic.com', 'Alif Hasan', '01565523644', 0, '2018-02-01 14:27:31'),
(2, 'Uttara Crescent Diagnostic & Consultation Center', 'H No. 16, Hannan Complex, Rabindra Sarani, Sector', '5', '8932430, 8933298, 01917704156', 'info@uttaracrescenthospitalbd.org', 'Dr. Nazmin Hossain', '01917704156', 11, '2018-02-01 14:33:23'),
(3, 'LABAID Diagnostic Center', 'House # 15, Road # 12, Sector # 6, Dhaka 1230', '4', '02-58950322', 'info@labaidgroup.com', 'Mr. XYZ', '01766661900', 11, '2018-02-01 14:40:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbtestcategoryinfo`
--

CREATE TABLE `tbtestcategoryinfo` (
  `id` int(11) NOT NULL,
  `testCategoryName` varchar(255) NOT NULL,
  `testCategoryDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtestcategoryinfo`
--

INSERT INTO `tbtestcategoryinfo` (`id`, `testCategoryName`, `testCategoryDescription`) VALUES
(1, 'Blood', 'None'),
(2, 'Urine', 'None'),
(3, 'X-Rays', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `tbtestinfo`
--

CREATE TABLE `tbtestinfo` (
  `id` int(11) NOT NULL,
  `testName` varchar(255) NOT NULL,
  `testDescription` text NOT NULL,
  `testCategoryId` varchar(10) NOT NULL,
  `testPrice` varchar(50) NOT NULL,
  `testLocation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtestinfo`
--

INSERT INTO `tbtestinfo` (`id`, `testName`, `testDescription`, `testCategoryId`, `testPrice`, `testLocation`) VALUES
(1, 'X-ray', 'none', '3', '3150', 'Room#401'),
(2, 'Abnormal Liver Enzymes (Liver Blood Tests)', 'None', '1', '240', 'Room#409'),
(3, 'Urine Test', 'None', '2', '300', 'Room#410'),
(6, 'Urine-HCG', '', '2', '390', 'Room#205'),
(7, 'Sugar (Urine)', '', '2', '340', 'Room#302'),
(8, 'Alpha-fetoprotein Blood Test', 'None', '1', '240', 'Room#409'),
(9, 'Blood Transfusion', 'None', '1', '240', 'Room#411'),
(10, 'Blood Group Test', 'This method is used for finding specific blood types in people. Specific proteins known as antigens on RBCs are the basis for finding blood types. The common blood group systems are ABO and Rh. Based on ABO system the blood group of a person can be divided into various types namely, A, B, AB and O, and as per Rh system, blood group is divided into positive and negative.', '1', '200', 'R#213'),
(11, 'Blood Sugar Test', 'Testing blood sugar is a vital tool for determining sugar present in blood; especially if you suffer from diabetes. It aids in treatment and prevention of diabetes in the long-run.', '1', '250', 'R#214');

-- --------------------------------------------------------

--
-- Table structure for table `tbtesttmp`
--

CREATE TABLE `tbtesttmp` (
  `id` int(11) NOT NULL,
  `tmpid` varchar(30) NOT NULL,
  `patientId` varchar(30) NOT NULL,
  `testId` int(11) NOT NULL,
  `amount` varchar(30) NOT NULL,
  `pbDate` date NOT NULL,
  `pbTime` varchar(30) NOT NULL,
  `referredAgentId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbtools`
--

CREATE TABLE `tbtools` (
  `id` int(11) NOT NULL,
  `myOption` varchar(255) NOT NULL,
  `myValue` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtools`
--

INSERT INTO `tbtools` (`id`, `myOption`, `myValue`) VALUES
(1, 'footertext1', '*** Slogan goes here ***');

-- --------------------------------------------------------

--
-- Table structure for table `tbusers`
--

CREATE TABLE `tbusers` (
  `userid` int(11) NOT NULL,
  `userFullName` varchar(200) NOT NULL,
  `userTypeId` int(1) NOT NULL,
  `userPhone` varchar(50) NOT NULL,
  `userEmail` varchar(200) NOT NULL,
  `userJoiningDate` varchar(50) NOT NULL,
  `userAddress` text NOT NULL,
  `userName` varchar(200) NOT NULL,
  `userPassword` text NOT NULL,
  `userImage` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbusers`
--

INSERT INTO `tbusers` (`userid`, `userFullName`, `userTypeId`, `userPhone`, `userEmail`, `userJoiningDate`, `userAddress`, `userName`, `userPassword`, `userImage`) VALUES
(4, 'Hasan ul ferdous', 4, '01824156689', 'hasan@feits.co', '01-01-2018', 'Uttara', 'hasan', 'fc3f318fba8b3c1502bece62a27712df', 'user20180124102827.jpg'),
(5, 'Tamim Bokhtiar', 4, '01824156559', 'tamim@feits.co', '01-01-2018', 'Uttara 11', 'tamim', '70693e458a02b1810e16112aad100f66', 'user20180124105446.jpg'),
(6, 'Hasan ul ferdous', 5, '01824156559', 'hasan1@feits.co', '01-01-2018', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 'laba', 'bbfcf4f1c86d2c6adcb3a4d7632bb16e', 'user2018013004485801824156559.jpg'),
(11, 'Hasan ul ferdous', 3, '01824156559', 'hasan6@feits.co', '01-01-2018', 'House #51, Road #18 Sector #11 Uttara, Dhaka-1230', 'recep', '746035f8741e922eb5fa31ea766d9ece', 'user2018013004485801824156559.jpg'),
(12, 'Admin', 1, '01824156689', '', '01-01-2018', 'Uttara', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'user20180124102827.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbdepartment`
--
ALTER TABLE `tbdepartment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `departmentName` (`departmentName`);

--
-- Indexes for table `tbdoctors`
--
ALTER TABLE `tbdoctors`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `tbdoctorshift`
--
ALTER TABLE `tbdoctorshift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbmedicalhistory`
--
ALTER TABLE `tbmedicalhistory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `patientId` (`patientId`);

--
-- Indexes for table `tbmedicine`
--
ALTER TABLE `tbmedicine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpatient`
--
ALTER TABLE `tbpatient`
  ADD PRIMARY KEY (`tmpId`);

--
-- Indexes for table `tbpatientappointment`
--
ALTER TABLE `tbpatientappointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpatientmedicine`
--
ALTER TABLE `tbpatientmedicine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpatientpayment`
--
ALTER TABLE `tbpatientpayment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbpatienttestdetails`
--
ALTER TABLE `tbpatienttestdetails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbreferral_agent`
--
ALTER TABLE `tbreferral_agent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtestcategoryinfo`
--
ALTER TABLE `tbtestcategoryinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtestinfo`
--
ALTER TABLE `tbtestinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtesttmp`
--
ALTER TABLE `tbtesttmp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbtools`
--
ALTER TABLE `tbtools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbusers`
--
ALTER TABLE `tbusers`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `userEmail` (`userEmail`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbdepartment`
--
ALTER TABLE `tbdepartment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbdoctors`
--
ALTER TABLE `tbdoctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbdoctorshift`
--
ALTER TABLE `tbdoctorshift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbmedicalhistory`
--
ALTER TABLE `tbmedicalhistory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbmedicine`
--
ALTER TABLE `tbmedicine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbpatient`
--
ALTER TABLE `tbpatient`
  MODIFY `tmpId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbpatientappointment`
--
ALTER TABLE `tbpatientappointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbpatientmedicine`
--
ALTER TABLE `tbpatientmedicine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tbpatientpayment`
--
ALTER TABLE `tbpatientpayment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbpatienttestdetails`
--
ALTER TABLE `tbpatienttestdetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `tbreferral_agent`
--
ALTER TABLE `tbreferral_agent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbtestcategoryinfo`
--
ALTER TABLE `tbtestcategoryinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbtestinfo`
--
ALTER TABLE `tbtestinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbtesttmp`
--
ALTER TABLE `tbtesttmp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbtools`
--
ALTER TABLE `tbtools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbusers`
--
ALTER TABLE `tbusers`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
