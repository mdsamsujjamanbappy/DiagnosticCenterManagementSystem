<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>HMS - Far-East IT Solutions Ltd.</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    
    <link href="assets/css/bootstrap-progressbar.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="vendors/animate.css/animate.min.css" rel="stylesheet">


    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">


    <?php 
      require_once("assets/tools/loginfunctions.php"); 
      $db_handle = new myLoginFunctions();                          

      if(isset($_POST['uname'])&&isset($_POST['upass'])){
           $uname = ($_POST['uname']);
           $upass = ($_POST['upass']);
           $type = ($_POST['userType']);


           if($type=="2"){

           	    $r = $db_handle->checkMSBDocLoginDetails($uname,$upass,$type);

	           if(count($r)>0){ 
	              $results = $db_handle->getMSBDocDetailsAtLogin($uname,$upass,$type);
	              foreach($results as $user) {
	                  $id=htmlentities($user["id"]); 
	                  $docFullName=htmlentities($user["name"]); 
	                  $docImage=htmlentities($user["image"]);
	              }

		              $_SESSION['docHMSAccess']="Permit";
		              $_SESSION['docHMSId']=$id;
		              $_SESSION['docHMSfname']=$docFullName; 
		              $_SESSION['docHMSImage'] = $docImage;
		              echo "<script> document.location.href='doctor/dashboard.php';</script>";

	             
	           }else{ 
	                  echo "<script> document.location.href='index.php?sts=failed';</script>";
	               }

           }else{
           	    $r = $db_handle->checkMSBUserLoginDetails($uname,$upass,$type);

	           if(count($r)>0){ 
	              $results = $db_handle->getMSBUserLoginDetails($uname,$upass,$type);
	              foreach($results as $user) {
	                  $id=htmlentities($user["userid"]); 
	                  $userFullName=htmlentities($user["userFullName"]); 
	                  $userTypeId=htmlentities($user["userTypeId"]);
	                  $userImage=htmlentities($user["userImage"]);
	              }

	              if($userTypeId==1){
		              $_SESSION['adHMSAccess']="Permit";
		              $_SESSION['adHMSId']=$id;
		              $_SESSION['adHMSfname']=$userFullName; 
		              $_SESSION['adHMSImage'] = $userImage;
		              echo "<script> document.location.href='admin/dashboard.php';</script>";
	              }

	              if($userTypeId==3){
		              $_SESSION['recpHMSAccess']="Permit";
		              $_SESSION['recpHMSId']=$id;
		              $_SESSION['recpHMSfname']=$userFullName; 
		              $_SESSION['recpHMSImage'] = $userImage;
		              echo "<script> document.location.href='receptionist/dashboard.php';</script>";
	              }


                if($userTypeId==4){
                  $_SESSION['medsHMSAccess']="Permit";
                  $_SESSION['medsHMSId']=$id;
                  $_SESSION['medsHMSfname']=$userFullName; 
                  $_SESSION['medsHMSImage'] = $userImage;
                  echo "<script> document.location.href='medical_store/dashboard.php';</script>";
                }

                if($userTypeId==5){
                  $_SESSION['labaHMSAccess']="Permit";
                  $_SESSION['labaHMSId']=$id;
                  $_SESSION['labaHMSfname']=$userFullName; 
                  $_SESSION['labaHMSImage'] = $userImage;
                  echo "<script> document.location.href='labattendant/dashboard.php';</script>";
                }

	             
	           }else{ 
	                  echo "<script> document.location.href='index.php?sts=failed';</script>";
	               }


           }
          }
          ?>

        <div class="animate form login_form" style="margin-top:-30px;">
          <section class="login_content">
            <img src="assets/hrms.png" alt="" width="50%" class="img">
            <form action="" method="POST">
              <h1>HMS Login Area</h1>
              <?php 
              if(isset($_GET['sts'])){ ?>

                  <div class="alert alert-danger alert-dismissible fade in" role="alert" style="color:white;font-size:13px;">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                   <span style="text-decoration:none;"> Login Failed ! Provide valid Information.</span>
                  </div>

              <?php } ?>
              <div>
                <input type="text" class="form-control" placeholder="Username" name="uname" required="" />
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" name="upass" required="" />
              </div>
              <div>
                  <select name="userType" class="form-control" required >
                    <option value="">Select User Type</option>
                    <option value="1">Admin</option>
                    <option value="2">Doctor</option>
                    <option value="3">Receptionist</option>
                    <!-- <option value="4">Medical Store Attendant</option> -->
                    <option value="5">LAB Attendant</option>
                    
                  </select>
              </div>
              <div style="padding-top:15px;">
                <center><input type="submit" class="btn btn-default" value="Log in" /></center>
              </div>

              <div class="clearfix"></div>

              <div class="separator">


                <div>
                  Copyright <?php echo date('Y');?> | <strong>Remedy Physiotherapy Hospital</strong> <br /> <br />
                    Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                </div>
                 <div>
                    HMS Version 1.0
                </div>
              </div>
            </form>
          </section>
        </div>

        <div id="register" class="animate form registration_form">
          <section class="login_content">
            <img src="assets/hrms.png" alt="" width="50%" class="img">
            <form action="#" method="POST">
              <center>
              <h1>Recover Account</h1>
              <div>
                <input type="text" required class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="email" required class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="submit" class="btn btn-default" value="Submit" />
              </div>
              </center>
              <div class="clearfix"></div>

              <div class="separator">
                <p class="change_link">
                  <a href="#signin" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br />

                <div>
                  Copyright <?php echo date('Y');?> | <strong>Remedy Physiotherapy Hospital</strong>
                  Developed by <a target="_BLANK" title="Far-East IT Solution Ltd." href="https://feits.co"> <strong>Far-East IT Solution Ltd.</strong></a>
                
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
