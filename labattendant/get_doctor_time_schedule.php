<?php
require_once 'header_link.php'; 
if(!empty($_POST["doctor_id"])) {
	$results1 = $db_handle->getDoctorTimeSchedule($_POST["doctor_id"]); ?>
	<table  class="table table-striped table-bordered table-hover" >
	<tr>
		<th>Date</th>
		<th>Day</th>
		<th>Time</th>
	</tr>

	<?php
	if(isset($results1)){
	 foreach($results1 as $time) { ?>

	<tr>
		<td><?php echo $time["apDate"]; ?></td>
		<td><?php echo date("l", strtotime($time["apDate"])); ?></td>
		<td><?php echo $time["apTime"]; ?></td>

	</tr>
<?php } }else{
		echo "<td colspan='3' align='center'>No Record Found</td>";
	} ?>
	</table>
<?php } ?>