<?php
require_once 'header_link.php'; 
if(!empty($_POST["doctor_id"])) {
	$results = $db_handle->getDoctorShift($_POST["doctor_id"]); ?>
	<table  class="table table-striped table-bordered table-hover" >
	<tr>
		<th>Date</th>
		<th>Time</th>
	</tr>
	<?php	
	if(isset($results)){
		foreach($results as $shift) {
?>
	<tr>
		<td><?php echo $shift["day"]; ?></td>
		<td><?php echo $shift["time"]; ?></td>
	</tr>
<?php } }else{
		echo "<td colspan='2' align='center'>No Record Found</td>";
	} ?>
	</table>
<?php } ?>