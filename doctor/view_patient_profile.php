<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $my_tools->title();?></title>
    <?php include("css.php");?>
    <script type="text/javascript" src="../assets/js/typeahead.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    $('#medicineName').typeahead({
        source: function (query, result) {
            $.ajax({
                url: "autocomplete_medicine_name.php",
                data: 'query=' + query,            
                dataType: "json",
                type: "POST",
                success: function (data) {
                    result($.map(data, function (item) {
                      return item;
                    }));
                }
            });
        }
    });

    });
    </script>
  </head>

  

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Doctor Panel</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->docImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->docFullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->docMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->docImage();?>" alt=""><?php $my_tools->docFullName();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->


        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            
            <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Patient Profile</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                  <?php 
                  $pid=000;
                    if(isset(($_GET['pid']))){
                      $_SESSION['pidd']=($_GET['pid']);
                    }

                   $pid= base64_decode($_SESSION['pidd']);
                   $results = $db_handle->getPatientDetails($pid);
                   if(isset($results)){ ?>
                     
                      <div id="div_print">

                      <div class="row" style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                        <div class="col-md-4 col-sm-12 col-xs-12" >
                          <table  class="table table-striped table-bordered table-hover" >
                           <?php 
                           $results = $db_handle->getPatientDetails($pid);
                              foreach($results as $pdetails) {
                            ?>
                            <tr>
                              <td width="35%">Patient ID</td>
                              <td><strong><?php echo $pdetails["id"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Name</td>
                              <td><strong><?php echo $pdetails["name"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Father/Husband</td>
                              <td><?php echo $pdetails["father"]; ?></td>
                            </tr>
                            <tr>
                              <td>Phone</td>
                              <td><?php echo $pdetails["phone"]; ?></td>
                            </tr>
                            <tr>
                              <td>Address</td>
                              <td><?php echo $pdetails["address"]; ?></td>
                            </tr>
                            <?php } ?>
                            </table>

                      </div>
                      <div class="col-md-4 col-sm-12 col-xs-12 no-print" >
                         <table  class="table table-striped table-bordered table-hover" >
                           <?php 
                           $results = $db_handle->getPatientDetails($pid);
                              foreach($results as $pdetails) {
                            ?> <tr>
                              <td>Occupation</td>
                              <td><?php echo $pdetails["occupation"]; ?></td>
                            </tr>
                            <tr>
                              <td width="35%">Gender</td>
                              <td><?php echo $pdetails["gender"]; ?></td>
                            </tr>
                            <tr>
                              <td>E-mail</td>
                              <td><?php echo $pdetails["email"]; ?></td>
                            </tr>
                            <tr>
                              <td>Blood Group</td>
                              <td><?php echo $pdetails["blood"]; ?></td>
                            </tr>
                            <tr>
                              <td>Age</td>
                              <td><?php echo $pdetails["age"]; ?></td>
                            </tr>
                            <tr>
                              <td>Date of Birth</td>
                              <td><?php echo $pdetails["dob"]; ?></td>
                            </tr>
                            <?php } ?>
                            </table>

                      </div>
                      <div class="col-md-4 col-sm-12 col-xs-12 no-print" style="color:black;">
                        <strong> Medical History</strong>
                        <ul>
                           <?php 
                           $PatientMedicalHistoryR = $db_handle->getPatientMedicalHistory($pid);
                           
                            if(isset($PatientMedicalHistoryR)){
                              foreach($PatientMedicalHistoryR as $PatientMedicalHistory) {
                              if($PatientMedicalHistory["hblood"]=="1"){
                                echo "<li> Blood Pressure </li>";
                              }
                              if($PatientMedicalHistory["hcardiac"]=="1"){
                                echo " <li> Cardiac Disease </li>";
                              }
                              if($PatientMedicalHistory["hrheumatic"]=="1"){
                                echo "<li> Rheumatic Fever </li>";
                              }
                              if($PatientMedicalHistory["hdiabetes"]=="1"){
                                echo "<li> Diabetes</li>";
                              }
                              if($PatientMedicalHistory["hhapatitis"]=="1"){
                                echo "<li> Hapatitis/Jaundice</li> ";
                              }
                              if($PatientMedicalHistory["hpregnancy"]=="1"){
                                echo "<li> Pregnancy</li>";
                              }
                              if($PatientMedicalHistory["hkidney"]=="1"){
                                echo "<li> Kidney Disease</li>";
                              }
                              if($PatientMedicalHistory["hhypersensitivity"]=="1"){
                                echo "<li> Hypersensitivity</li>";
                              } 
                              if($PatientMedicalHistory["hbloodDisorder"]=="1"){
                                echo "<li> Blood Disorder </li>";
                              }
                              if($PatientMedicalHistory["hcancer"]=="1"){
                                echo "<li> Cancer </li>";
                              }
                            }
                            ?>
                            </ul>
                            <?php }else{
                              echo "No data found.";
                            } ?>

                      </div>
                      </div>
                     
                      

                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #c5c5c5;padding:12px;">
                          <table  id="datatable" class="table table-striped table-bordered table-hover" style="font-size:12px;" >
                          <thead>
                              <th  width="5%">SN</th>
                              <th  width="14%">Test Name</th>
                              <th  width="10%">Date</th>
                              <th  width="10%">Time</th>
                              <th width="25%"> Test Result Description </th>
                              <th width="15%"> Test Result Attachment</th>
                            </thead>
                            <tbody>
                           <?php 
                           $results1 = $db_handle->getPatientTestList($pid);
                           $i=0;
                           if(count($results1)>0){
                              foreach($results1 as $dataArr) {
                            ?> 
                            <tr>
                              <td><?php echo ++$i; ?></td>
                              <td><?php echo $dataArr['testName']; ?></td>
                              <td><?php echo date('d-m-Y', strtotime($dataArr['testDate'])); ?></td>
                              <td><?php echo $dataArr['testTime']; ?></td>
                              <td>
                              <?php if(!empty($dataArr['testRDescription'])){ ?>
                                <?php echo htmlentities(substr($dataArr['testRDescription'], 0, 200)); ?>
                              <?php
                              if(!empty(substr($dataArr['testRDescription'], 200))){
                                  echo "...... <a class='btn btn-success btn-xs btn-round' data-toggle='tooltip' href='view_patient_test_details.php?id=".base64_encode($dataArr['id'])."&&pid=".base64_encode($pid)."' title='Read More..' rel='facebox'><i class='fa fa-eye'></i></a>";
                              }


                               }else{ echo " ";}?>
                              </td>
                              <td>
                              <?php if(!empty($dataArr['testAttachment'])){ ?>
                                <a class="btn btn-primary btn-sm" target="_BLANK" href="../test_report/<?php echo $dataArr['testAttachment']; ?>"> <i class="fa fa-download"></i> View / Download</a>
                              <?php }else{ echo " ";}?>
                              </td>
                              
                            </tr>
                            <?php }
                            } ?>
                            </tbody>
                            </table>

                        </div>
                      </div>
                      <br > <br >
                      <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="border:1px solid #c5c5c5;padding:12px;">
                        <center><button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-chrome"></i> Prescribe New Medicine</button></center> 
                        <hr>
                          <table id="datatable1" class="table table-striped table-bordered table-hover" >
                          <thead>
                              <th  style="text-align:center;"  width="10%">Date</th>
                              <th  style="text-align:center;" >Medicine</th>
                          </thead>
                          <tbody>
                           <?php 
                           $results1 = $db_handle->getPatientMedicineDate($pid);
                           if(isset($results1)){
                              foreach($results1 as $PatientMedicineDate) {
                            ?> 
                            <tr>
                              <td><?php echo date('d-m-Y', strtotime($PatientMedicineDate['pdate'])); ?>

                              <br />
                              <br />
                              </td>
                              <td >
                          <table class="table table-striped table-bordered table-hover" >
                                  <tr>
                                    <th  width="5%">SN</th>
                                    <th>Name Of Medicine</th>
                                    <th>Uses</th>
                                    <th>Days</th>
                                    <th width="35%">Instuction</th>
                                  </tr>
                                 <?php 
                                 $i=0;
                                   $results2 = $db_handle->getPatientMedicine($pid,$PatientMedicineDate['pdate']);
                                   if(isset($results2)){
                                      foreach($results2 as $PatientMedicine) {
                                    ?> 
                                    <tr>
                                      <td><?php echo ++$i; ?></td>
                                      <td><?php echo $PatientMedicine['medicine']; ?></td>
                                      <td><?php echo $PatientMedicine['uses']; ?></td>
                                      <td><?php echo $PatientMedicine['days']; ?></td>
                                      <td><?php echo $PatientMedicine['instruction']; ?></td>
                                    </tr> 
                                <?php } } ?>
                                </table>
                            </tr>
                            <?php 
                              }
                              } ?>
                              </tbody>
                            </table>

                          </div>
                      </div>

                      </div>

                     <?php }else{ ?>
                     <h1 style='color:red;' ><strong>Invalid Patient ID !</strong></h1><br /><br />
                     <a href="patient_profile.php" class="btn btn-primary "><i class="fa fa-undo"></i> Go Back</a> 
                     <?php } ?>
                     
          
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
        <!-- /page content -->
          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
               
               <form action="save_prescription.php" method="POST" accept-charset="utf-8">

                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel">Prescribe New Medicine</h4>
                </div>
                    <div class="modal-body" style="padding:10px 80px 0px 80px;">
                          
                        <div class="form-group">
                           <input type="text" name="_MSBtoken" hidden value="_MSBtoken" id="id">
                        </div>
                        <div class="form-group">
                          <label for="MedicineName">Medicine Name</label>
                           <input id="medicineName" name="medicineName" autofocus required class="form-control" placeholder="Enter Medicine Name here " >
                          
                          </div>

                        <div class="form-group">
                          <label for="Uses">Uses</label>
                          <input id="Uses" name="uses" required class="form-control" placeholder="Enter Medicine Uses " >
                        </div>

                        <div class="form-group">
                          <label for="Day">Duration Day</label>
                          <input id="Day" name="duration" class="form-control" id="auto" required  placeholder="Enter Number of days to continue " >
                        </div>

                        <div class="form-group">
                          <label for="Instruction">Instruction</label> 
                          <input id="Instruction" class="form-control" name="instruction" required   placeholder="Enter Instuction ">
                        </div>

                    </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>

              </div>
            </div>
          </div>


        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    
  </body>
</html>