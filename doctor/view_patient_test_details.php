<?php 
require_once 'header_link.php'; 
?>
<div>
    <div class="row animated " style="color:black;">
        <div class="col-md-12">
            <table class="table table-striped table-bordered responsive-utilities">
              <?php
                $tid=base64_decode($_GET['id']);
                $pid=base64_decode($_GET['pid']);
                $results1 = $db_handle->getPatientTestDetailsById($tid,$pid);
                if(count($results1)>0){
                  foreach($results1 as $dataArr) {
              ?>
              <tr>
                <td width="25%"><b>Description:</b></td>
                <td><?php echo htmlentities($dataArr['testRDescription']);?></td>
              </tr>
              <tr>
                <td><b>Attachment:</b></td>
                <td>
                  <a class="btn btn-primary btn-sm" target="_BLANK" href="../test_report/<?php echo $dataArr['testAttachment']; ?>"> <i class="fa fa-download"></i> View / Download</a>
                  
                </td>
              </tr>
              <?php }} ?>
            </table>    
         </div>
    </div>
</div>