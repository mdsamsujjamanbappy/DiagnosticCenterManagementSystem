<?php
require_once 'header_link.php';
$name = ($_POST['name']);
$phone = ($_POST['phone']);
$email = ($_POST['email']);
$occupation = ($_POST['occupation']);
$father = ($_POST['father']);
$address = ($_POST['address']);
$gender = ($_POST['gender']);
$blood = ($_POST['blood']);
$age = ($_POST['age']);
$dob = ($_POST['dob']);
$doctor_id = ($_POST['doctor_id']);
$apDate = ($_POST['apDate']);
$apTime = ($_POST['apTime']);
$appFee = ($_POST['appFee']);

if(isset($_POST['hblood'])){
	$hblood=1;
}else{
	$hblood=0;
}

if(isset($_POST['hcardiac'])){
	$hcardiac=1;
}else{
	$hcardiac=0;
}

if(isset($_POST['hrheumatic'])){
	$hrheumatic=1;
}else{
	$hrheumatic=0;
}

if(isset($_POST['hdiabetes'])){
	$hdiabetes=1;
}else{
	$hdiabetes=0;
}

if(isset($_POST['hhapatitis'])){
	$hhapatitis=1;
}else{
	$hhapatitis=0;
}

if(isset($_POST['hpregnancy'])){
	$hpregnancy=1;
}else{
	$hpregnancy=0;
}

if(isset($_POST['hkidney'])){
	$hkidney=1;
}else{
	$hkidney=0;
}

if(isset($_POST['hhypersensitivity'])){
	$hhypersensitivity=1;
}else{
	$hhypersensitivity=0;
}

if(isset($_POST['hbloodDisorder'])){
	$hbloodDisorder=1;
}else{
	$hbloodDisorder=0;
}

if(isset($_POST['hcancer'])){
	$hcancer=1;
}else{
	$hcancer=0;
}

date_default_timezone_set('Asia/Dhaka');
$rnd = rand(0,9);
$id=date("Ymdhis").$rnd;
$date=date("Y-m-d");
$recepId=$_SESSION['recpHMSId'];
$r = $db_handle->savePatient($id,$name,$phone,$email,$address,$date,$occupation,$father,$gender,$blood,$age,$dob);
$r = $db_handle->savePMedicalHistory($id,$hblood,$hcardiac,$hrheumatic,$hdiabetes,$hhapatitis,$hpregnancy,$hkidney,$hhypersensitivity,$hbloodDisorder,$hcancer);
$r1 = $db_handle->saveAppointment($id,$apDate,$apTime,$doctor_id,$appFee,$date,$recepId);

if(isset($_POST['isPaid'])){
	$db_handle->saveAppointmentPayment($id,$date,$appFee,$appFee,"Consultation Fee");
}else{
	$db_handle->saveAppointmentPayment($id,$date,0,$appFee,"Consultation Fee");

}


 if($r&&$r1){
		echo "<script> document.location.href='conappointment.php?pid=".base64_encode($id)."&&sts=insertSuccess';</script>";
 }else{
		echo "<script> document.location.href='conappointment.php?pid=".base64_encode($id)."&&stf=insertFailed';</script>";
 }
?>