<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Appointment</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.theme.min.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.min.css" rel="stylesheet">
    <script>                              
        function getDoctorTime(val) {
        $.ajax({
        type: "POST",
        url: "get_doctor_shift.php",
        data:'doctor_id='+val,
        success: function(data){
        $("#doctor_schedule").html(data);
        }
        });
        $.ajax({
        type: "POST",
        url: "get_doctor_time_schedule.php",
        data:'doctor_id='+val,
        success: function(data){
        $("#doctor_time_schedule").html(data);
        }
        });
        $.ajax({
          type: "POST",
          url: "get_doctor_consultation_fee.php",
          data:'doctor_id='+val,
          success: function(data){
              $("#doctor_consultation_fee").html(data);
          }
          });
        }
                  
      </script>  
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Receptionist</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->recpImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->recpFullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->recpMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->recpImage();?>" alt=""><?php $my_tools->recpFullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Appointment</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                    <form action="save_consultation_appointment.php" method="POST" >
                      <div class="row"  style="border:1px solid #c5c5c5;padding:12px;" >
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="form-group">
                              <label for="emloyeeName">Fullname * </label>
                              <input id="emloyeeName" autofocus type="text" class="form-control" required name="name" placeholder="Please Enter Fullname Here" />
                            </div> 
                            <div class="form-group">
                              <label for="Phone">Phone *</label>
                              <input id="Phone" type="text" class="form-control" required name="phone" placeholder="Please Enter Phone Number Here" />
                            </div>
                            
                             <div class="form-group">
                              <label for="Occupation">Occupation</label>
                              <input id="Occupation" type="text" class="form-control"  name="occupation" placeholder="Please Enter Occupation Here " />
                            </div>
                             <div class="form-group">
                              <label for="Father">Father/Husband</label>
                              <input id="Father" type="text" class="form-control" name="father" placeholder="Please Enter Father/Husband Here" />
                            </div>
                            
                            <div class="form-group">
                              <label for="address">Address * </label>
                              <textarea id="address" class="form-control"  placeholder="Please Enter Address Here"  required name="address"></textarea>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                              <div class="form-group">
                              <label for="Gender">Gender * </label><br>
                              <input id="Gender" type="radio" value="Male" required name="gender" /> Male  &nbsp;&nbsp;| &nbsp;&nbsp; 
                              <input id="Gender" type="radio"  value="Female" name="gender" /> Female&nbsp;&nbsp;|&nbsp;&nbsp;
                              <input id="Gender" type="radio" value="Other" name="gender" /> Other&nbsp;&nbsp;&nbsp;
                            </div>
                            <div class="form-group">
                              <label for="email">E-mail</label>
                              <input id="email" type="email" class="form-control" name="email" placeholder="Please Enter E-mail Here" />
                            </div>
                            
                            <div class="form-group">
                              <label for="Blood">Blood Group</label>
                              <input id="Blood" type="text" class="form-control" name="blood"   placeholder="Please Enter Blood Group Here" >
                            </div> 
                            <div class="form-group">
                              <label for="Age">Age</label>
                              <input id="Age" type="text" class="form-control"  name="age" placeholder="Please Enter Age Here" />
                            </div>
                             <div class="form-group">
                              <label for="dob">Date of Birth</label>
                              <input id="dob" type="text" class="form-control"  name="dob" placeholder="Please Enter Date of Birth Here " />
                            </div>
                             
                        </div>
                      </div>
                      <hr />  
                      <h4>Medical History</h4>
                      <div class="row"  style="border:1px solid #c5c5c5;padding:12px;" >
                        <div class="col-md-5 col-md-offset-1 col-sm-12 col-xs-12">
                            <div class="form-group">
                              <input type="checkbox" name="hblood" id="hblood"> <label for="hblood"> Blood Pressure</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hcardiac" id="Cardiac"> <label for="Cardiac"> Cardiac Disease</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hrheumatic" id="Rheumatic"> <label for="Rheumatic"> Rheumatic Fever</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hdiabetes" id="Diabetes"> <label for="Diabetes"> Diabetes</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hhapatitis" id="Hapatitis"> <label for="Hapatitis"> Hapatitis/Jaundice</label>
                            </div> 
                           
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12">
                            <div class="form-group">
                              <input type="checkbox" name="hpregnancy" id="Pregnancy"> <label for="Pregnancy"> Pregnancy</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hkidney" id="Kidney"> <label for="Kidney"> Kidney Disease</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hhypersensitivity" id="Hypersensitivity"> <label for="Hypersensitivity"> Hypersensitivity</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hbloodDisorder" id="BloodDisorder"> <label for="BloodDisorder"> Blood Disorder</label>
                            </div> 
                            <div class="form-group">
                              <input type="checkbox" name="hcancer" id="Cancer"> <label for="Cancer"> Cancer</label>
                            </div> 
                        </div>
                      </div>
                      <hr />
                      <div class="row">
                        <div class="col-md-5  col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;">
                            
                            <div class="form-group">
                              <label for="email">Select Doctor</label>
                               <select name="doctor_id" class="form-control" required onChange="getDoctorTime(this.value);" >
                                  <option value=""> Select Doctor</option>
                                    <?php
                                      $results = $db_handle->getDoctorList();
                                   foreach($results as $doctor) {
                                    ?>
                                       <option value="<?php echo $doctor['id']; ?>"><?php echo $doctor['name']; ?></option>
                                    <?php } ?>
                                </select>

                                 
                            </div>
                            <div class="form-group">
                              <label for="Date">Date</label>
                              <input id="Date" type="text" class="form-control" required name="apDate" placeholder="Please Enter Date Here " />
                            </div>
                            <div class="form-group">
                              <label for="Time">Time</label>
                              <input id="Time" type="text" class="form-control" required name="apTime" placeholder="Ex. 09:00 AM" />
                            </div>
                            <div class="form-group" style="margin-top:15px;">
                              <label for="Fee">Appointment Fee</label>
                              <div id="doctor_consultation_fee">
                                <input class="form-control" disabled placeholder="Appointment fee will be automatically placed here..  " >
                              </div>
                            </div>
                            <hr>
                            <div class="form-group">
                              <input  type="checkbox" id="paid" name="isPaid" checked > Paid Amount
                            </div>
                            <hr>
                          <button type="submit"  align="center" class="btn btn-primary btn-md">Save Appointment</button>

                        </div> 
                        <div class="col-md-offset-1  col-md-6 col-sm-12 col-xs-12" style="border:1px solid #c5c5c5;padding:12px;">
                           
                             <h5 style="font-weight:bold;">Doctor Shift</h5>
                             <div  id="doctor_schedule"> <span style="color:#90908F;">Doctor shift details will be shown here.</span></div>
                             <hr>
                             <h5 style="font-weight:bold;">Booked Appointment</h5>
                             <div  id="doctor_time_schedule"><span style="color:#90908F;">Booked appointment details will be shown here.</span></div>
                        
                        </div>
                           
                      </div>
                   </form>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../assets/js/jquery-ui.min.js"></script>
      <script>
  $( function() {
    $( "#Date" ).datepicker({
      dateFormat: "yy-mm-dd",
      minDate: 0
    });
    $( "#dob" ).datepicker({
      dateFormat: "yy-mm-dd"
    });

  } );
  </script>

  </body>
</html>