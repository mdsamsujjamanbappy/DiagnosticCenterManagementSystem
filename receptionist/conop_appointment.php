<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Appointment</title>

    <?php include("css.php");?>
    
    <!-- bootstrap-daterangepicker -->
    <link href="../vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="../vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

    <script>                              
        function getTestDetails(val) {
        $.ajax({
        type: "POST",
        url: "get_testdetails.php",
        data:'test_id='+val,
        success: function(data){
        $("#testDetails").html(data);
        }
        });
        }

        function getTestItemList(val) {
          $.ajax({
          type: "POST",
          url: "get_test_list.php",
          data:'category_id='+val,
          success: function(data){
          $("#test_list").html(data);
          }
          });
        } 
        </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Receptionist</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->recpImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->recpFullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->recpMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->recpImage();?>" alt=""><?php $my_tools->recpFullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Appointment</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">


                  <?php 
                   $results = $db_handle->getPatientDetails($_SESSION["patientId"]);
                   if(isset($results)){ ?>
                      <div class="row" style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                        <div class="col-md-4 col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                          <table  class="table table-striped table-bordered table-hover" >
                           <?php 
                           $results = $db_handle->getPatientDetails($_SESSION["patientId"]);
                              foreach($results as $pdetails) {
                            ?>
                            <tr>
                              <td width="35%">Patient ID</td>
                              <td><strong><?php echo $pdetails["id"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Patient Name</td>
                              <td><strong><?php echo $pdetails["name"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Patient Phone</td>
                              <td><strong><?php echo $pdetails["phone"]; ?></strong></td>
                            </tr>
                            <?php } ?>
                            </table>

                      </div>
                      <div class="col-md-7 col-md-offset-1 col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                           <?php if(isset($_GET['stf'])){ ?>
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <strong>Insertion Failed !</strong>
                            </div>
                            <?php } ?>
                          <table  class="table table-striped table-bordered table-hover" >
                            <tr>
                              <td width="5%">SN</td>
                              <td>Test Name</td>
                              <td>Amount</td>
                              <td>Date</td>
                              <td>Time</td>
                              <td width="3%"></td>
                            </tr>
                           <?php 
                           $i=0;
                           $tot=0;
                           $results = $db_handle->getTmpOpTestList($_SESSION["patientId"],$_SESSION["tmpOpId"]);
                           if(isset($results)){
                              foreach($results as $pdetails) {
                                $tot+=$pdetails["amount"];
                            ?>
                            <tr>
                              <td><?php echo ++$i; ?></td>
                              <td><strong><?php echo $pdetails["testName"]; ?></strong></td>
                              <td><?php echo $pdetails["amount"]; ?> TK</td>
                              <td><?php echo $pdetails["pbDate"]; ?></td>
                              <td><?php echo $pdetails["pbTime"]; ?></td>
                              <td>
                               <center><a data-toggle="tooltip" title="Remove Test" onclick="return confirm('Are you sure to remove this test?')" href="remove_test_appointment.php?iid=<?php echo md5($pdetails["id"]);?>&&id=<?php echo base64_encode($pdetails["id"]);?>" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a></center> 
                              </td>
                            </tr>
                            <?php } 
                            $_SESSION['opTot']=$tot; ?>
                            <tr>
                              <td colspan="6"><center><h5>Total Amount: <?php echo $tot; ?> Taka</h5></center></td>
                            </tr>
                            <tr>
                             <td  colspan="6"> <center><a href="askforpayment.php" title="" class="btn btn-primary"><i class="fa fa-save"></i> Save Appointment</a></center></td>
                            </tr>

                            <?php }else{ ?>
                             <tr>
                              <td colspan="6"><strong></strong></td>
                            </tr>
                             <?php } ?>
                            </table>

                      </div>
                      </div>
                      <form action="save_op_appointment.php" method="POST" >
                      <div class="row">
                        <div class="col-md-5  col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;">
                            <div class="form-group">
                              <label for="email">Select Test Category</label>
                              <select class="form-control" required onChange="getTestItemList(this.value);" >
                                  <option value=""> Select Test Category </option>
                                    <?php
                                      $results = $db_handle->getTestCategoryList();
                                   foreach($results as $TestList) {
                                    ?>
                                       <option value="<?php echo $TestList['id']; ?>"><?php echo $TestList['testCategoryName']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                              <label for="email">Select Test Item</label>
                               <select name="test_id" id="test_list"  onChange="getTestDetails(this.value);"  class="form-control" required ></select>
                            </div>
                            <div class="form-group">
                              <label for="Date">Test Date</label>
                              <input  id="single_cal4" type="text" class="form-control" required name="pbDate" placeholder="Please Enter Date Here " />
                            </div>
                            <div class="form-group">
                              <label for="Time">Test Time</label>
                              <input id="Time" type="text" class="form-control" required name="pbTime" placeholder="Ex. 09:00 AM" />
                            </div>
                            <div class="form-group">
                              <label for="email">Referred  By</label>
                               <select name="referredBy" class="form-control" >
                                  <option value="0">No Reference</option>
                                    <?php
                                      $results = $db_handle->getReferralAgentList();
                                   foreach($results as $ReferralAgentList) {
                                    ?>
                                       <option value="<?php echo $ReferralAgentList['id']; ?>"><?php echo $ReferralAgentList['name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <hr>
                            <button type="submit"  align="center" class="btn btn-primary btn-md"><i class="fa fa-eject"></i> Add To List</button>
                        </div> 
                        <div class="col-md-offset-1  col-md-6 col-sm-12 col-xs-12" style="border:1px solid #c5c5c5;padding:12px;">
                            <h5 style="font-weight:bold;">Test Details</h5>
                            <hr>
                             <div  id="testDetails"> <span  style="color:#90908F;"> Test details will be appeared here</span></div>
                        </div>
                      </div>
                     </form>
                     <?php }else{ ?>
                     <h1 style='color:red;' ><strong>Invalid Patient ID !</strong></h1><br /><br />
                     <a href="op_app.php" class="btn btn-primary ">Go Back</a> 
                     <?php } ?>




                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
   
    <!-- bootstrap-daterangepicker -->
    <script src="../vendors/moment/min/moment.min.js"></script>
    <script src="../vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
     <!-- bootstrap-datetimepicker -->    
    <script src="../vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../assets/js/jquery-ui.min.js"></script>
      <script>
        $( function() {
          $( "#Date" ).datepicker({
            dateFormat: "yy-mm-dd",
            minDate: 0
          });
          $( "#dob" ).datepicker({
            dateFormat: "yy-mm-dd"
          });

        } );
      </script>

  </body>
</html>