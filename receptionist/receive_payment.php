<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Patient Payment</title>

    <?php include("css.php");?>

    <!-- Datatables -->
    <link href="../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link href="../vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="../vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.theme.min.css" rel="stylesheet">
    <link href="../assets/css/jquery-ui.min.css" rel="stylesheet">
    <script type="text/javascript">
    $(document).ready(function(){
    $("#pid").keyup(function() {
    var pid = $('#pid').val();
    if(pid=="")
    {
    $("#disp").html("");
    }
    else
    {
    $.ajax({
      type: "POST",
      url: "check_pid.php",
      data: "pid="+ pid ,
    success: function(html){
      $("#disp").html(html);
    }
    });

    return false;
    }
    });
    });
    </script>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Receptionist</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->recpImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->recpFullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->recpMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->recpImage();?>" alt=""><?php $my_tools->recpFullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Patient Payment</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                     <form action="" method="POST"  >
                        <div class="row"  >
                          <div class="col-md-6 col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;">
                              <div class="form-group">
                                <label for="patientId">Patient ID</label>
                                <input  id="pid" autofocus type="number" class="form-control" required name="patientId" placeholder="Please Enter Patient ID Here" />
                              <div style='margin-top:15px;' id="disp"></div>
                              </div>
                              
                               <div class="form-group">
                                <button type="submit" class="btn btn-primary" > <i class="fa fa-chevron-circle-right"></i> Continue</button>
                              </div>
                          </div>
                        </div>
                      </form>

                      <hr>

                      <?php 
                        if(isset($_POST['patientId'])){

                            $result = $db_handle->checkPatientId($_POST['patientId']);
                            if($result==0){
                              echo "<h2 style='color:red;'>Patient ID not found.</h2>";
                            }else{
                                  echo "<script> document.location.href='patient_payment.php?id=".md5($_POST['patientId'])."&&pid=".base64_encode($_POST['patientId'])."';</script>";

                            }

                        }
                      ?>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
    
    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="../vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="../assets/js/jquery-ui.min.js"></script>
      <script>
  $( function() {
    $( "#Date" ).datepicker({
      dateFormat: "yy-mm-dd",
      minDate: 0
    });
    $( "#dob" ).datepicker({
      dateFormat: "yy-mm-dd"
    });

  } );
  </script>

  </body>
</html>