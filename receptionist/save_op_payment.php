<?php
require_once 'header_link.php'; 
$amount = ($_POST['amount']);
$tpp = $db_handle->getTotalRowNumber('tbpatientpayment');
$paymentId =$tpp+1; 
date_default_timezone_set('Asia/Dhaka');
$date=date("Y-m-d");
$recepId=$_SESSION['recpHMSId'];
$r = $db_handle->savePayment($paymentId,$_SESSION["patientId"],$amount,$_SESSION['opTot'],$date,"Payment Against Tests","1");

$results = $db_handle->getTmpOpTestList($_SESSION["patientId"],$_SESSION["tmpOpId"]);
foreach($results as $tdetails) {
 	$db_handle->saveToPb($_SESSION["patientId"],$tdetails["testId"],$tdetails["amount"],$tdetails["pbDate"],$tdetails["pbTime"],$recepId,$date,$tdetails["referredAgentId"],$paymentId);
}
$db_handle->clearDataFromTmpOpTest($_SESSION["patientId"],$_SESSION["tmpOpId"]);

$pid=$_SESSION["patientId"];
unset($_SESSION["patientId"]);
unset($_SESSION["tmpOpId"]);
unset($_SESSION["opTot"]);

 if($r){
	echo "<script> document.location.href='conf_op_app.php?pid=".base64_encode($pid)."&&payid=".base64_encode($paymentId)."&&sts=insertSuccess';</script>";
 }else{
	echo "<script> document.location.href='conf_op_app.php?stf=insertFailed';</script>";
 }
?>