<?php
ob_start();
require_once 'header_link.php';
require('../fpdf/fpdf.php');
class PDF extends FPDF
{
	function Header()
	{
		$this->SetX(5);
		$this->SetTextColor(0, 0, 0);
		$this ->SetFont('Helvetica','B',20);;
		$this->Cell(198,8,invoiceCompanyTitle(),0,0,'R');
		$this->Ln(8);
		
		$this->SetTextColor(0, 0, 0);
		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyAddress(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(40,10,"",0,0,'L');
		$this->Cell(58,10,"",0,0,'L');
		$this->Cell(100,10,invoiceCompanyPhone(),0,0,'R');
		$this->Ln(6);

		$this->SetX(5);
		$this ->SetFont('Times','',11);;
		$this->Cell(198,10,invoiceCompanyEmail(),0,0,'R');
		$this->Ln(9);

		// Header line
		$this->SetX(35);
		$this->SetAlpha(0.9);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,42,202,42);
		$this->Ln(4);

		// Logo Image
		$this->SetAlpha(1);
		$image=mainLogo();
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,15,5,35,35);

		// Background Watermark Image
		$this->SetAlpha(0.09);
		$image=watermarkImages();
		$this-> SetFont("Arial","B",10);
		$this-> Image($image,35,75,130,130);
	}

	function Footer()
	{

		// Footer line
	    $this->SetY(-25);
		$this->SetAlpha(1);
		$this ->SetDrawColor(183, 183, 183);
		$this ->SetFont('Times','',11);
		$this->Line(8,270,202,270);
		$this->Ln(9);

	    $this->SetY(-18);
		$this->SetAlpha(0.6);
	    $this->SetFont('Arial','I',12);
	    $this->Cell(90,10,footerText1(),0,0,'L');
	    $this->Cell(100,10,footerText2(),0,0,'R');
	    
	}

	var $extgstates = array();

    function SetAlpha($alpha, $bm='Normal')
    {
        // set alpha for stroking (CA) and non-stroking (ca) operations
        $gs = $this->AddExtGState(array('ca'=>$alpha, 'CA'=>$alpha, 'BM'=>'/'.$bm));
        $this->SetExtGState($gs);
    }

    function AddExtGState($parms)
    {
        $n = count($this->extgstates)+1;
        $this->extgstates[$n]['parms'] = $parms;
        return $n;
    }

    function SetExtGState($gs)
    {
        $this->_out(sprintf('/GS%d gs', $gs));
    }

    function _enddoc()
    {
        if(!empty($this->extgstates) && $this->PDFVersion<'1.4')
            $this->PDFVersion='1.4';
        parent::_enddoc();
    }

    function _putextgstates()
    {
        for ($i = 1; $i <= count($this->extgstates); $i++)
        {
            $this->_newobj();
            $this->extgstates[$i]['n'] = $this->n;
            $this->_out('<</Type /ExtGState');
            $parms = $this->extgstates[$i]['parms'];
            $this->_out(sprintf('/ca %.3F', $parms['ca']));
            $this->_out(sprintf('/CA %.3F', $parms['CA']));
            $this->_out('/BM '.$parms['BM']);
            $this->_out('>>');
            $this->_out('endobj');
        }
    }

    function _putresourcedict()
    {
        parent::_putresourcedict();
        $this->_out('/ExtGState <<');
        foreach($this->extgstates as $k=>$extgstate)
            $this->_out('/GS'.$k.' '.$extgstate['n'].' 0 R');
        $this->_out('>>');
    }

    function _putresources()
    {
        $this->_putextgstates();
        parent::_putresources();
    }
}


$pdf = new PDF();
$pdf->AddPage();



$filename = "Blank-pad".".pdf";

$pdf->Output("",$filename,"false");

ob_end_flush();
?>
