<?php
require_once 'header_link.php'; 
if(!empty($_POST["test_id"])) {
	$results = $db_handle->getTestDetails($_POST["test_id"]); ?>
	<table  class="table table-striped table-bordered table-hover" >

	<?php	
	if(isset($results)){
		foreach($results as $tdetails) {
?>
	<tr>
		<td width="30%">Test Name</td>
		<td><strong><?php echo $tdetails["testName"]; ?></strong></td>
	</tr>
	<tr>
		<td>Test Price</td>
		<td><strong><?php echo $tdetails["testPrice"]; ?> Taka</strong></td>
	</tr>
	<tr>
		<td width="30%">Test Category</td>
		<td><strong><?php echo $tdetails["testCategoryName"]; ?></strong></td>
	</tr>
	<tr>
		<td>Test Description</td>
		<td><?php echo $tdetails["testDescription"]; ?></td>
	</tr>
	<tr>
		<td>Test Location</td>
		<td><strong><?php echo $tdetails["testLocation"]; ?></strong></td>
	</tr>


<?php } }else{
		echo "<td colspan='2' align='center'>No Record Found</td>";
	} ?>
	</table>
<?php } ?>