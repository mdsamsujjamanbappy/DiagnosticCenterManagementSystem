<?php require_once 'header_link.php'; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Ask for payment</title>

    <?php include("css.php");?>
  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="dashboard.php" class="site_title"><i class="fa fa-th"></i> <span>Receptionist</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo $my_tools->recpImage();?>" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2> <?php $my_tools->recpFullname();?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Main Menu </h3>
                <?php $my_tools->recpMenu();?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
                 <?php $my_tools->sidebar_footer();?>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo $my_tools->recpImage();?>" alt=""><?php $my_tools->recpFullname();?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                 <?php $my_tools->profileLink();?>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Appointment</h3>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_content">
                      <div class="row">
                        <div class="col-md-5 col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                          <table  class="table table-striped table-bordered table-hover" >
                           <?php 
                           $results = $db_handle->getPatientDetails($_SESSION["patientId"]);
                              foreach($results as $pdetails) {
                            ?>
                            <tr>
                              <td width="35%">Patient ID</td>
                              <td><strong><?php echo $pdetails["id"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Patient Name</td>
                              <td><strong><?php echo $pdetails["name"]; ?></strong></td>
                            </tr>
                            <tr>
                              <td>Patient Phone</td>
                              <td><strong><?php echo $pdetails["phone"]; ?></strong></td>
                            </tr>
                            <?php } ?>
                            </table>
                      </div>

                      <form action="save_op_payment.php" method="POST"  >
                            <div class="col-md-offset-1 col-md-6 col-sm-12 col-xs-12"  style="border:1px solid #c5c5c5;padding:12px;margin-bottom:10px;">
                                 <div class="form-group">
                                   <h3>Total Payable Amount: <strong><?php echo $_SESSION['opTot'];?> Taka</strong></h3>
                                 </div>
                                 <div class="form-group">
                                  <label for="Amount">Amount</label>
                                  <input autofocus id="Amount" type="number" min="0" step="any" class="form-control" required name="amount" placeholder="Please Enter Money Amount Here " />
                                </div>
                                 <div class="form-group">
                                  <button type="submit" class="btn btn-primary" value="" ><i class="fa fa-money"></i> Save Payment</button>
                                </div>
                            </div>
                        </form>
                    </div>


                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
          <?php $my_tools->myFooter();?>
        <!-- /footer content -->
      </div>
    </div>
    <?php include("js.php");?>
   
  </body>
</html>